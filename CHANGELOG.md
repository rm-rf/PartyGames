# v 2.0.0

## Changelog(de):

TL;DR
Die App wurde komplett neu geschrieben und auf das neue Designframework von Google migriert.

### Technische Änderungen
- Jetpack Compose
- Material You Farben
- Neue Art die Einstellungen zu speichern
- Umstellung auf MVVM



### Sonstige Änderungen
- Die Sprache kann jetzt ab Android 13+ über die Android-Einstellungen geändert werden
- Mytos oder Fakt wurde entfernt, da wir die korrektheit der Aussagen nicht zu 100 % gewährleisten konnen
- Einige Spielmodi wurden noch nicht re-implementiert, das kommt aber noch


## Changelog(en):

TL;DR
The app has been completely rewritten and migrated to Google's new design framework.

### Technical Changes
- Jetpack Compose
- Material You Color
- New method of saving settings
- Switch to MVVM

### Other Changes
- The language can now be changed via the Android settings from Android 13+
- Myth or Fact has been removed as we cannot guarantee the correctness of the statements 100%
- Some game modes have not been re-implemented yet, but they are coming soon


# v 1.6.1

## Changelog(de):

### - Crash beim starten der app behoben.

## Changelog(en):

### - Fixed crash on app startup.

# v 1.6.0

## Changelog(de):

### - Neue Spiele.

- Guess the Song wurde hinzugefügt.
- Flaschendrehen wurde hinzugefügt.

### - UI Changes:

- Update zu Material3
- Es werden nur noch die eigenen Lieblingsspiele im Main-menu angezeigt. Der rest ist natürlich noch in der Seitenleiste
  zu finden

### - Danke für die neuen Übersetzungen:

- mondstern (Polnisch)
- anthony (Französisch)
- Olman Quiros Jimenez (Spanish)
- Степан (Russich)
- LopsemPyier (Französisch)

## Changelog(en):

### - New games.

- Guess the Song was added.
- Spin the Bottle was added.

### - UI Changes:

- Update to Material3
- Now only your favorite games are shown in the main menu. The rest is of course still in the sidebar.

### - Thanks for the new translations:

- mondstern (Polish)
- anthony (French)
- Olman Quiros Jimenez (Spanish)
- Степан (Russian)
- LopsemPyier (French)

# v 1.5.2

## Changelog(de):

### - Fixed:

- Fragen in Wahrheit oder Pflicht Party waren vertauscht.

### - Danke für die neuen Übersetzungen:

- Marco De Marchi (Italienisch)

## Changelog(en):
### - Fixed:
- Questions in Truth or Dare Party were swapped

### - Thanks for the new Translations:
- Marco De Marchi (Italian)

# v 1.5.1
## Changelog(de):

### - Fixed:
- Layout Fehler auf kleinen Displays.
- Layout Fehler in Wahrheit oder Pflicht.
- Fehler in würdest du eher, bei dem sich die Spieler nicht geändert haben.
- Fehler beim alle Fragen zurücksetzten, bei dem die App abstürzte.

### - Updated:
- App Screenshots

## Changelog(en):
### - Fixed:
- Layout issue on smaller displays.
- Layout issue in TruthOrDare.
- Bug in WouldYouRather, where the player wasn't changing.
- Bug at all question reset, causing an app crash.

### - Updated:
- App Screenshots

# v 1.5.0
## Changelog(de):

### - Die Spieler verwaltung wurde überarbeitet.
- Spieler werden jetzt in einer data class gespeichert.

### - UI Changes:
- Ein navigation drawer wurde für bessere Erreichbarkeit hinzugefügt..
- Die Schriftart wurde zu rubik geändert
- Ein SplashScreen wurde hinzugefügt
- Das App-Theme wurde überarbeitet

### - LicenseView:
- Die eigene Lizenz und die Lizenz der Schriftart wurde geändert.

## Changelog(en):
### - Player management has been revised.
- Players are now stored in a data class.

### - UI Changes:
- Added navigation drawer for better accessibility.
- Updated font to rubik
- Added a SplashScreen
- updated app theme

### - LicenseView:
- Added own app license and font license

# v 1.4.0
## Changelog(de):

### - Das Fragenmanagement wurde überarbeitet.
- Beantwortete Fragen werden jetzt nicht mehr verwendet.
- Nun besteht die Möglichkeit eigene Fragen lokal hinzuzufügen.

### - UI-Changes:
- Die Karte mit den Fragen hat von uns ein neues Aussehen bekommen.
- Die Texte sind jetzt endlich richtig zentriert.
- Die Karte in Mythos oder Fakt zeigt jetzt das richtige Ergebnis an, indem sie die Farbe ändert.

### - LicenseView:
- Jetzt können endlich auch in der App alle Lizenzen richtig angeguckt werden.

### - Danke an alle neuen Übersetzer:
- Allan Nordhøy (Norwegian Bokmål)
- Michael Moroni (Italian)
- Samuel (Spanish)
- Twann (French)
- GunnarZ (French)

### - Danke an alle Übersetzer, die vor dieser Version übersetzt haben:
- mondstern (French, Norwegian Bokmål)
- rendeiwave (French)

## Changelog(en):

### - Question management has been revised.
- Answered questions are now no longer used.

### - UI Changes:
- The card with the questions got a new look from us.
- The texts are now finally centered correctly.
- The card in Myth or Fact now shows the correct result by changing color.

### - LicenseView:
- Now all licenses can finally be viewed correctly in the app.

### - Thanks to all new translators:
- Allan Nordhøy (Norwegian Bokmål)
- Michael Moroni (Italian)
- Samuel (Spanish)
- Twann (French)
- GunnarZ (French)

### - Thanks to all translators before this version:
- mondstern (French, Norwegian Bokmål)
- rendeiwave (French)

# v 1.3.1
- Code cleanup
- Bug fixes
- Added Translation support for the Questions

# v 1.3.0
## Changelog (en)

* Added pantomime and partnerpantomime.
* Added optional timers for: Truth or dare, myth or fact, would you rather and pantomime
* Added French translations
* Added about screens for the games and the contributors
* Added myth or fact multiplayer + scores
* Added landscape layouts

## Changelog (de)

* Pantomime und Partnerpantomime hinzugefügt.
* Optionale Timer hinzugefügt für: Wahrheit oder Pflicht, Mythos oder Fakt, Würdest du eher und Pantomime
* Französische Übersetzungen hinzugefügt
* Info-Bildschirme für die Spiele und die Mitwirkenden hinzugefügt
* Mythos oder Fakt Multiplayer + Punkte
* Querformat-Layouts hinzugefügt
