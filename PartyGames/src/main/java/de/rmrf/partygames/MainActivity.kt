package de.rmrf.partygames

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import androidx.navigation.compose.rememberNavController
import de.rmrf.partygames.ui.components.AppScaffold
import de.rmrf.partygames.ui.components.ConfigureAppBar
import de.rmrf.partygames.ui.components.Drawer
import de.rmrf.partygames.ui.screens.Screen
import de.rmrf.partygames.ui.screens.about.AboutScreen
import de.rmrf.partygames.ui.screens.aboutlibraries.AboutLibrariesScreen
import de.rmrf.partygames.ui.screens.guessthesong.GuessTheSongScreen
import de.rmrf.partygames.ui.screens.main.MainScreen
import de.rmrf.partygames.ui.screens.neverhaveiever.NeverHaveIEverScreen
import de.rmrf.partygames.ui.screens.pantomime.PantomimeScreen
import de.rmrf.partygames.ui.screens.preferences.FavoritesPreference
import de.rmrf.partygames.ui.screens.preferences.PreferencesGameScreen
import de.rmrf.partygames.ui.screens.preferences.PreferencesMainScreen
import de.rmrf.partygames.ui.screens.preferences.PreferencesUIScreen
import de.rmrf.partygames.ui.screens.spinthebottle.SpinTheBottleScreen
import de.rmrf.partygames.ui.screens.truthordare.TruthOrDareScreen
import de.rmrf.partygames.ui.screens.whowouldrather.WhoWouldRatherScreen
import de.rmrf.partygames.ui.screens.wouldyourather.WouldYouRatherScreen
import de.rmrf.partygames.ui.theme.AppTheme
import de.rmrf.partygames.ui.viewmodel.main.AppBarState
import org.koin.core.component.KoinComponent

class MainActivity : ComponentActivity(), KoinComponent {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AppTheme {
                navigation()
            }
        }
    }
}


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainActivity.navigation() {
    val navController = rememberNavController()
    var appBarState by remember {
        mutableStateOf(AppBarState())
    }
    var destination by remember {
        mutableStateOf<String?>(Screen.MainScreen.route)
    }
    val scope = rememberCoroutineScope()
    val drawerState = rememberDrawerState(initialValue = DrawerValue.Closed)
    val composeAppBarState = rememberTopAppBarState()
    val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(
        state = composeAppBarState
    )
    val onActionClicked = remember {
        mutableStateOf({})
    }
    navController.addOnDestinationChangedListener { _, navDestination, _ ->
        destination = navDestination.route
    }

    Drawer(
        drawerState = drawerState,
        destination = destination,
        scope = scope,
        navController = navController
    ) {
        AppScaffold(
            modifier = Modifier.nestedScroll(scrollBehavior.nestedScrollConnection),
            topBar = {
                TopAppBar(
                    title = {
                        Text(text = appBarState.title)
                    },
                    actions = {
                        appBarState.actions?.invoke(this)
                    },
                    navigationIcon = {
                        appBarState.navigationIcon?.invoke(drawerState)
                    },
                    colors = TopAppBarDefaults.mediumTopAppBarColors(
                        containerColor = MaterialTheme.colorScheme.surface
                    ),
                    scrollBehavior = scrollBehavior
                )
            }
        ) { innerPadding ->
            Box(
                modifier = Modifier
                    .padding(innerPadding)
                    .fillMaxSize()
            ) {
                ConfigureAppBar(
                    navController,
                    scope,
                    drawerState,
                    destination,
                    onActionClick = onActionClicked,
                    appBarScrollBehavior = scrollBehavior
                ) {
                    appBarState = it
                }
                NavHost(
                    enterTransition = {
                        if (destination?.contains("preferences_screen") == false) {
                            slideInHorizontally(initialOffsetX = {it})
                        } else {
                            fadeIn()
                        }

                    },
                    popEnterTransition = {
                        if (destination?.contains("preferences_screen") == false) {
                            slideInHorizontally(initialOffsetX = {-it})
                        } else {
                            fadeIn()
                        }
                    },
                    exitTransition = {
                        if (destination?.contains("preferences_screen") == false) {
                            slideOutHorizontally(targetOffsetX = {-it})
                        } else {
                            fadeOut()
                        }

                    },
                    popExitTransition = {
                        if (destination?.contains("preferences_screen") == false) {
                            slideOutHorizontally(targetOffsetX = {it})
                        } else {
                            fadeOut()
                        }
                    },
                    navController = navController,
                    startDestination = Screen.MainScreen.route,
                ) {
                    composable(
                        route = Screen.MainScreen.route,
                    ) {
                        MainScreen(
                            navController = navController
                        )
                    }
                    composable(
                        route = Screen.LibraryScreen.route
                    ) {
                        AboutLibrariesScreen()
                    }
                    composable(
                        route = Screen.PantomimeScreen.route,
                    ) {
                        PantomimeScreen(navController = navController)
                    }
                    composable(
                        route = Screen.SpinTheBottleScreen.route
                    ) {
                        SpinTheBottleScreen(navController = navController)
                    }
                    composable(
                        route = Screen.NeverHaveIEverScreen.route
                    ) {
                        NeverHaveIEverScreen(navController = navController)
                    }
                    composable(
                        route = Screen.WhoWouldRatherScreen.route
                    ) {
                        WhoWouldRatherScreen(navController = navController)
                    }
                    composable(
                        route = Screen.WouldYouRatherScreen.route
                    ) {
                        WouldYouRatherScreen(navController = navController)
                    }
                    composable(
                        route = Screen.PreferencesScreen.FavoritesScreen.route
                    ) {
                        FavoritesPreference()
                    }
                    composable(
                        route = Screen.GuessTheSongScreen.route
                    ) {
                        GuessTheSongScreen(
                            navController = navController,
                            onActionClicked = onActionClicked
                        )
                    }
                    composable(
                        route = Screen.TruthOrDareScreen.route
                    ) {
                        TruthOrDareScreen(navController = navController)
                    }
                    navigation(
                        startDestination = Screen.PreferencesScreen.PreferencesMainScreen.route,
                        enterTransition = null,
                        exitTransition = null,
                        popEnterTransition = null,
                        popExitTransition = null,
                        route = Screen.PreferencesScreen.PreferencesScreenDefault.route
                    ) {
                        composable(
                            route = Screen.PreferencesScreen.PreferencesMainScreen.route,
                            enterTransition = null,
                            exitTransition = null,
                            popEnterTransition = null,
                            popExitTransition = null,
                        ) {
                            PreferencesMainScreen(navController = navController)
                        }
                        composable(
                            route = Screen.PreferencesScreen.PreferencesScreenGames.route,
                            enterTransition = null,
                            exitTransition = null,
                            popEnterTransition = null,
                            popExitTransition = null,
                        ) {
                            PreferencesGameScreen()
                        }
                        composable(
                            route = Screen.PreferencesScreen.PreferencesUIScreen.route,
                            enterTransition = null,
                            exitTransition = null,
                            popEnterTransition = null,
                            popExitTransition = null,
                        ) {
                            PreferencesUIScreen()
                        }
                    }
                    composable(
                        route = Screen.AboutScreen.route
                    ) {
                        AboutScreen(navController = navController)
                    }
                }
            }
        }
    }
}
