package de.rmrf.partygames.preferences

import android.content.Context
import android.os.Build
import androidx.compose.ui.graphics.toArgb
import de.rmrf.partygames.ui.screens.preferences.components.materialColors

val Context.dataStorePreferences: DataStorePreferences
    get() = this.run {
        DataStorePreferences(
            enablePantomimeScore = booleanDataStore(
                "preference_pantomime_score"
            ),
            enablePantomimePartnerMode = booleanDataStore(
                "preference_pantomime_partner"
            ),
            pantomimeTimerValue = intDataStore(
                "preference_pantomime_timer_value"
            ),
            enableOversteppedPantomimeCountsAsUsed = booleanDataStore(
                "preference_pantomime_all_count_as_used"
            ),
            favoriteGames = stringSetDataStore(
                "preference_favorites",
                emptySet()
            ),

            enableWouldYouRather18Mode = this.booleanDataStore(
                "preference_wouldyourather_18"
            ),
            enableWouldYouRatherTimer = this.booleanDataStore(
                "preference_wouldyourather_timer"
            ),
            wouldYouRatherTimerValue = this.intDataStore(
                "preference_wouldyourather_timer_value"
            ),

            enableWhoWouldRather18Mode = this.booleanDataStore(
                "preference_whowouldrather_18"
            ),
            enableWhoWouldRatherTimer = this.booleanDataStore(
                "preference_whowouldrather_timer"
            ),
            whoWouldRatherTimerValue = this.intDataStore(
                "preference_whowouldrather_timer_value"
            ),

            enableTruthOrDare18Mode = this.booleanDataStore(
                "preference_truthordare_18"
            ),
            enableTruthOrDareTimer = this.booleanDataStore(
                "preference_truthordare_timer"
            ),
            truthOrDareTimerValue = this.intDataStore(
                "preference_truthordare_timer_value"
            ),

            firstStart = this.booleanDataStore(
                "first_start",
                false
            ),

            appVersions = this.stringSetDataStore(
                "app_versions",
            ),

            firstGuesstheSongStart = this.booleanDataStore(
                key = "preference_guessthesong_first_start",
                true
            ),

            enableNeverHaveIEver18Mode = this.booleanDataStore(
                "preference_neverhaveiever_18"
            ),

            themeColor = this.intDataStore(
                "preference_theme_color",
                defaultValue = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
                    resources.getColor(android.R.color.system_accent1_500, theme)
                else
                    materialColors[0].toArgb()
            ),
            darkTheme = this.stringDataStore(
                "preference_dark_theme",
                dependencyValue = { it != "off" }
            ),
            darkThemeOled = this.booleanDataStore(
                "preference_dark_theme_oled"
            ),
            enableSimpleMainScreen = this.booleanDataStore(
                "preference_simple_main_screen"
            )
        )
    }


class DataStorePreferences(
    val enablePantomimeScore: PartyGamesPreferenceDataStore<Boolean>,
    val enablePantomimePartnerMode: PartyGamesPreferenceDataStore<Boolean>,
    val enableOversteppedPantomimeCountsAsUsed: PartyGamesPreferenceDataStore<Boolean>,
    val pantomimeTimerValue: PartyGamesPreferenceDataStore<Int>,
    val favoriteGames: PartyGamesPreferenceDataStore<Set<String>>,

    val enableWouldYouRather18Mode: PartyGamesPreferenceDataStore<Boolean>,
    val enableWouldYouRatherTimer: PartyGamesPreferenceDataStore<Boolean>,
    val wouldYouRatherTimerValue: PartyGamesPreferenceDataStore<Int>,

    val enableWhoWouldRather18Mode: PartyGamesPreferenceDataStore<Boolean>,
    val enableWhoWouldRatherTimer: PartyGamesPreferenceDataStore<Boolean>,
    val whoWouldRatherTimerValue: PartyGamesPreferenceDataStore<Int>,

    val enableTruthOrDare18Mode: PartyGamesPreferenceDataStore<Boolean>,
    val enableTruthOrDareTimer: PartyGamesPreferenceDataStore<Boolean>,
    val truthOrDareTimerValue: PartyGamesPreferenceDataStore<Int>,

    val firstStart: PartyGamesPreferenceDataStore<Boolean>,
    val firstGuesstheSongStart: PartyGamesPreferenceDataStore<Boolean>,

    val appVersions: PartyGamesPreferenceDataStore<Set<String>>,

    val enableNeverHaveIEver18Mode: PartyGamesPreferenceDataStore<Boolean>,

    val themeColor: PartyGamesPreferenceDataStore<Int>,
    val darkTheme: PartyGamesPreferenceDataStore<String>,
    val darkThemeOled: PartyGamesPreferenceDataStore<Boolean>,
    val enableSimpleMainScreen: PartyGamesPreferenceDataStore<Boolean>,
)
