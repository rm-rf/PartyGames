package de.rmrf.partygames.preferences

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*


val Context.preferenceDataStore: DataStore<Preferences> by androidx.datastore.preferences.preferencesDataStore(
    name = "preferences"
)
val Context.globalDataStore: DataStore<Preferences> by androidx.datastore.preferences.preferencesDataStore(
    name = "global"
)

fun Context.intDataStore(
    key: String,
    defaultValue: Int = resources.getInteger(
        resources.getIdentifier(
            "${key}_default",
            "integer",
            packageName
        )
    ),
    dependencyValue: (prefValue: Int) -> Boolean = { it != 0 },
    subDependency: PartyGamesPreferenceDataStore<*>? = null
): PartyGamesPreferenceDataStore<Int> {
    return PartyGamesPreferenceDataStore(
        dataStore = preferenceDataStore,
        prefKey = intPreferencesKey(key),
        defaultValue = defaultValue,
        dependencyValue = dependencyValue,
        subDependency = subDependency
    )
}

fun Context.doubleDataStore(
    key: String,
    defaultValue: Double = resources.getInteger(
        resources.getIdentifier(
            "${key}_default",
            "integer",
            packageName
        )
    ).toDouble(),
    dependencyValue: (prefValue: Double) -> Boolean = { it != 0.0 },
    subDependency: PartyGamesPreferenceDataStore<*>? = null
): PartyGamesPreferenceDataStore<Double> {
    return PartyGamesPreferenceDataStore(
        dataStore = preferenceDataStore,
        prefKey = doublePreferencesKey(key),
        defaultValue = defaultValue,
        dependencyValue = dependencyValue,
        subDependency = subDependency
    )
}

fun Context.stringDataStore(
    key: String,
    defaultValue: String = resources.getString(
        resources.getIdentifier(
            "${key}_default",
            "string",
            packageName
        )
    ),
    dependencyValue: (prefValue: String) -> Boolean = { it.isNotBlank() },
    subDependency: PartyGamesPreferenceDataStore<*>? = null
): PartyGamesPreferenceDataStore<String> {
    return PartyGamesPreferenceDataStore(
        dataStore = preferenceDataStore,
        prefKey = stringPreferencesKey(key),
        defaultValue = defaultValue,
        dependencyValue = dependencyValue,
        subDependency = subDependency
    )
}

fun Context.booleanDataStore(
    key: String,
    defaultValue: Boolean = resources.getBoolean(
        resources.getIdentifier(
            "${key}_default",
            "bool",
            packageName
        )
    ),
    dependencyValue: (prefValue: Boolean) -> Boolean = { it },
    subDependency: PartyGamesPreferenceDataStore<*>? = null
): PartyGamesPreferenceDataStore<Boolean> {
    return PartyGamesPreferenceDataStore(
        dataStore = preferenceDataStore,
        prefKey = booleanPreferencesKey(key),
        defaultValue = defaultValue,
        dependencyValue = dependencyValue,
        subDependency = subDependency
    )
}

fun Context.floatDataStore(
    key: String,
    defaultValue: Float = resources.getInteger(
        resources.getIdentifier(
            "${key}_default",
            "integer",
            packageName
        )
    ).toFloat(),
    dependencyValue: (prefValue: Float) -> Boolean = { it != 0f },
    subDependency: PartyGamesPreferenceDataStore<*>? = null
): PartyGamesPreferenceDataStore<Float> {
    return PartyGamesPreferenceDataStore(
        dataStore = preferenceDataStore,
        prefKey = floatPreferencesKey(key),
        defaultValue = defaultValue,
        dependencyValue = dependencyValue,
        subDependency = subDependency
    )
}

fun Context.longDataStore(
    key: String,
    defaultValue: Long = resources.getInteger(
        resources.getIdentifier(
            "${key}_default",
            "integer",
            packageName
        )
    ).toLong(),
    dependencyValue: (prefValue: Long) -> Boolean = { it != 0L },
    subDependency: PartyGamesPreferenceDataStore<*>? = null
): PartyGamesPreferenceDataStore<Long> {
    return PartyGamesPreferenceDataStore(
        dataStore = preferenceDataStore,
        prefKey = longPreferencesKey(key),
        defaultValue = defaultValue,
        dependencyValue = dependencyValue,
        subDependency = subDependency
    )
}

fun Context.stringSetDataStore(
    key: String,
    defaultValue: Set<String> = emptySet(),
    dependencyValue: (prefValue: Set<String>) -> Boolean = { it.isNotEmpty() },
    subDependency: PartyGamesPreferenceDataStore<*>? = null
): PartyGamesPreferenceDataStore<Set<String>> {
    return PartyGamesPreferenceDataStore(
        dataStore = preferenceDataStore,
        prefKey = stringSetPreferencesKey(key),
        defaultValue = defaultValue,
        dependencyValue = dependencyValue,
        subDependency = subDependency
    )
}
