package de.rmrf.partygames.ui.screens

sealed class Screen(val route: String) {
    data object MainScreen : Screen("main_screen")
    data object PantomimeScreen : Screen("pantomime_screen")
    data object SpinTheBottleScreen : Screen("spinthebottle_screen")
    data object NeverHaveIEverScreen : Screen("neverhaveiever_screen")
    data object WhoWouldRatherScreen : Screen("whowouldrather_screen")
    data object WouldYouRatherScreen : Screen("wouldyourather_screen")
    data object TruthOrDareScreen : Screen("truthordare_screen")
    data object GuessTheSongScreen : Screen("guessthesong_screen")

    sealed class PreferencesScreen(prefRoute: String) : Screen("preferences_screen/$prefRoute") {
        data object FavoritesScreen : PreferencesScreen("favorites")

        data object PreferencesMainScreen : PreferencesScreen("main")

        data object PreferencesScreenDefault : PreferencesScreen("")

        data object PreferencesUIScreen : PreferencesScreen("ui")

        data object PreferencesScreenGames : PreferencesScreen("games")
    }

    data object LibraryScreen : Screen("libraries_screen")

    data object AboutScreen : Screen("about_screen")

}
