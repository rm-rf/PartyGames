package de.rmrf.partygames.ui.theme

import androidx.compose.foundation.background
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.luminance
import androidx.compose.ui.platform.LocalContext
import com.google.accompanist.systemuicontroller.SystemUiController
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import de.rmrf.partygames.preferences.dataStorePreferences
import de.rmrf.partygames.ui.components.conditional
import de.rmrf.partygames.ui.screens.preferences.bottomInsets
import kotlinx.coroutines.flow.cancellable
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.launch

@Composable
fun AppTheme(
    initialDarkTheme: Boolean = isSystemInDarkTheme(),
    navBarInset: Boolean = true,
    systemUiController: SystemUiController? = rememberSystemUiController(),
    content: @Composable () -> Unit
) {
    val context = LocalContext.current
    val scope = rememberCoroutineScope()

    val themeColorPref = context.dataStorePreferences.themeColor
    val darkThemePref = context.dataStorePreferences.darkTheme
    val darkThemeOledPref = context.dataStorePreferences.darkThemeOled

    var customThemeColor by remember { mutableStateOf<Color?>(null) } // Workaround to allow legacy views to respond to theme color changes
    var colorScheme by remember { mutableStateOf<ColorScheme?>(null) }

    DisposableEffect(true) {
        val job = scope.launch {
            combine(
                themeColorPref.getValueFlow().cancellable(),
                darkThemePref.getValueFlow().cancellable(),
                darkThemeOledPref.getValueFlow().cancellable()
            ) { themeColor, darkTheme, darkThemeOled ->
                val darkThemeBool = when (darkTheme) {
                    "on" -> true
                    "off" -> false
                    else -> initialDarkTheme
                }

                systemUiController?.let {
                    setSystemUiColor(it, Color.Transparent, !darkThemeBool)
                }

                val dynamicColor = themeColor == themeColorPref.defaultValue
                customThemeColor = if (dynamicColor) null else Color(themeColor)

                generateColorScheme(
                    context,
                    dynamicColor,
                    Color(themeColor),
                    darkTheme = darkThemeBool,
                    darkThemeOled
                )
            }.collect {
                colorScheme = it
            }
        }

        onDispose {
            job.cancel()
        }
    }

    colorScheme?.let {
        MaterialTheme(
            colorScheme = it,
        ) {
            Surface(
                modifier = Modifier
                    .background(MaterialTheme.colorScheme.background)
                    .conditional(navBarInset) {
                        bottomInsets()
                    }
                    .windowInsetsPadding(WindowInsets.systemBars.only(WindowInsetsSides.Horizontal + WindowInsetsSides.Top)),
                content = content
            )
        }
    }
}

fun setSystemUiColor(
    systemUiController: SystemUiController,
    color: Color = Color.Transparent,
    darkIcons: Boolean = color.luminance() > 0.5f
) {
    systemUiController.run {
        setSystemBarsColor(
            color = color,
            darkIcons = darkIcons
        )

        setNavigationBarColor(
            color = color,
            darkIcons = darkIcons
        )
    }
}
