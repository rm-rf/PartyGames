package de.rmrf.partygames.ui.screens.preferences.components

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.*
import androidx.compose.ui.res.stringResource
import de.rmrf.partygames.R
import de.rmrf.partygames.preferences.PartyGamesPreferenceDataStore

@Composable
fun ConfirmDialogPreference(
    title: (@Composable () -> Unit),
    dialogTitle: (@Composable () -> Unit) = title,
    summary: (@Composable () -> Unit)? = null,
    dialogText: (@Composable () -> Unit)? = summary,
    icon: (@Composable () -> Unit)? = null,
    dependency: PartyGamesPreferenceDataStore<*>? = null,
    onConfirm: () -> Unit = {}
) {
    var showDialog by remember { mutableStateOf(false) }

    Preference(
        title = title,
        summary = summary,
        icon = icon,
        dependency = dependency,
        dataStore = PartyGamesPreferenceDataStore.emptyDataStore(),
        onClick = { showDialog = true }
    )

    if (showDialog) {
        AlertDialog(
            onDismissRequest = { showDialog = false },
            title = dialogTitle,
            text = dialogText,
            confirmButton = {
                TextButton(
                    onClick = {
                        showDialog = false
                        onConfirm()
                    }) {
                    Text(stringResource(R.string.all_ok))
                }
            },
            dismissButton = {
                TextButton(onClick = { showDialog = false }) {
                    Text(stringResource(id = R.string.all_cancel))
                }
            }
        )
    }
}
