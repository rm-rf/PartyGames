package de.rmrf.partygames.ui.viewmodel.guessthesong

import android.content.Context
import android.os.CountDownTimer
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.exoplayer.ExoPlayer
import de.rmrf.partygames.data.entities.SongData
import de.rmrf.partygames.data.entities.api.PipedAPI
import de.rmrf.partygames.data.song.SongRepository
import de.rmrf.partygames.util.TimerBaseViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.core.component.inject


/**
 * ViewModel class for the GuessTheSong feature.
 * Extends TimerBaseViewModel for timer functionality.
 *
 * @property songRepository The repository for accessing song data.
 * @property _state A mutable state property for holding the current state of the GuessTheSong feature.
 * @property state The state property exposed as an immutable State object.
 * @property player An instance of ExoPlayer for playing media items.
 * @property millisInFuture The total time for the countdown timer.
 */
class GuessTheSongViewModel(
    private val songRepository: SongRepository
) : TimerBaseViewModel() {

    private val _state = mutableStateOf(SongState())
    val state: State<SongState> = _state

    private val _nextSongData = mutableStateOf<SongData?>(null)

    private val player = ExoPlayer.Builder(inject<Context>().value).build()

    private val listener = object : Player.Listener {
        override fun onIsPlayingChanged(isPlaying: Boolean) {
            super.onIsPlayingChanged(isPlaying)
            if (isPlaying) {
                startTimer()
            }
        }
    }


    init {
        millisInFuture = 10 * 1000
        timeData.value = millisInFuture
        viewModelScope.launch {
            _state.value = state.value.copy(
                showSongPickerDialog = true,
                availablePlaylist = songRepository.getAllPlaylitst()
            )
        }
        player.addListener(listener)
    }

    fun dismissDialog() {
        _state.value = state.value.copy(
            showSongPickerDialog = false
        )
    }

    /**
     * Retrieves new song data from the song repository and updates the state.
     * TODO: Preload on wait.
     */
    private fun getNewSongData() {
        viewModelScope.launch {
            _nextSongData.value = songRepository.getSong(state.value.currentPlaylist?.playlistId ?: return@launch)
            if (state.value.songData == null) {
                _state.value = state.value.copy(
                    songData = _nextSongData.value
                )
            }
            fetchSongData()
        }
    }

    /**
     * Fetches the song data from YouTube and prepares the media player.
     *
     * If the song data is available in the [state]'s songData property, it extracts the YouTube URL using the songId.
     * It then checks if the extraction is successful. If successful, it updates the [state] by setting isLoading to false.
     * It adds the media item using the YouTube URL (YTFiles) and prepares the media player.
     *
     * If the extraction is unsuccessful, it recursively calls itself to retry fetching the song data.
     *
     * @throws Exception if an error occurs during the fetching process
     * @see [state]
     */
    private suspend fun fetchSongData() {
        player.addMediaItem(MediaItem.fromUri(songRepository.getBestStream(_nextSongData.value ?: return)))
        player.prepare()
        player.pause()
        player.selectRandomPosition()
        _state.value = state.value.copy(
            isSongLoading = false
        )
    }

    /**
     * Starts the player by selecting a random position and then playing.
     */
    private fun startPlayer() {
        player.play()
        viewModelScope.launch {
            _state.value = state.value.copy(
                videoData = state.value.songData?.videoFromId(inject<PipedAPI>().value)
            )
        }
    }


    /**
     * Stops the player and clears the media items.
     */
    private fun stopPlayer() {
        player.stop()
        player.clearMediaItems()
    }

    /**
     * Creates a new [CountDownTimer] object with specified parameters.
     *
     * @return the created [CountDownTimer] object.
     */

    override fun createTimerObject(): CountDownTimer {
        return object : CountDownTimer(millisInFuture, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timeData.value = millisUntilFinished
            }

            override fun onFinish() {
                onEvent(GuessTheSongEvent.ShowAnswer)
                running.value = false
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        stopPlayer()
        player.release()
    }

    /**
     * Handles events related to the GuessTheSong feature.
     *
     * @param guessTheSongEvent The event to handle.
     */
    fun onEvent(guessTheSongEvent: GuessTheSongEvent) {
        when (guessTheSongEvent) {
            GuessTheSongEvent.ShowAnswer -> {
                cancelTimer()
                stopPlayer()
                viewModelScope.launch {
                    _state.value = state.value.copy(
                        showSolution = true
                    )
                    getNewSongData()
                    delay(3500)
                    _state.value = state.value.copy(
                        showSolution = false,
                        songData = _nextSongData.value
                    )
                }
            }

            GuessTheSongEvent.ActionIconClicked -> {
                _state.value = state.value.copy(showSongPickerDialog = true)
                stopPlayer()
            }

            GuessTheSongEvent.TimerStart -> {
                startPlayer()
            }

            is GuessTheSongEvent.DialogEvent -> {
                onDialogEvent(event = guessTheSongEvent)
            }
        }
    }


    private fun onDialogEvent(event: GuessTheSongEvent.DialogEvent) {
        when (event) {
            is GuessTheSongEvent.DialogEvent.AddPlaylist -> {
                viewModelScope.launch {
                    _state.value = state.value.copy(isAddingPlaylist = true)
                    val job = viewModelScope.launch {
                        songRepository.addPlaylist(event.playlistId)
                    }
                    job.join()
                    _state.value = state.value.copy(
                        isAddingPlaylist = false,
                        availablePlaylist = songRepository.getAllPlaylitst()
                    )
                }
            }

            is GuessTheSongEvent.DialogEvent.DeletePlaylist -> {
                viewModelScope.launch {
                    val job = viewModelScope.launch {
                        songRepository.deletePlaylist(event.playlist.playlistId)
                    }
                    job.join()
                    _state.value = state.value.copy(
                        availablePlaylist = songRepository.getAllPlaylitst()
                    )
                }
            }
            is GuessTheSongEvent.DialogEvent.SelectPlaylist -> {
                _state.value = state.value.copy(
                    showSongPickerDialog = false,
                    currentPlaylist = event.playlist,
                )
                getNewSongData()
            }
        }
    }

    /**
     * Selects a random position within the audio playback duration and seeks to that position.
     */
    private fun ExoPlayer.selectRandomPosition() {
        this.seekTo((4000..(this.duration - 16000)).random())
    }


}
