package de.rmrf.partygames.ui.screens.main

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavController
import de.rmrf.partygames.preferences.dataStorePreferences
import de.rmrf.partygames.ui.screens.main.components.FirstStartDialog
import de.rmrf.partygames.ui.screens.main.components.GameGrid
import de.rmrf.partygames.ui.screens.main.components.GameList
import de.rmrf.partygames.ui.viewmodel.main.MainViewModel
import org.koin.androidx.compose.koinViewModel

@Composable
fun MainScreen(
    navController: NavController,
    mainViewModel: MainViewModel = koinViewModel()
) {

    val dataStorePreferences = LocalContext.current.dataStorePreferences

    if (dataStorePreferences.firstStart.getState().value) {
        FirstStartDialog()
    }

    val items by mainViewModel.list
    val simpleMainScreen = dataStorePreferences.enableSimpleMainScreen.getState()
    if (simpleMainScreen.value) {
        GameList(
            navController = navController,
            items = items,
            modifier = Modifier.fillMaxSize()
        ) {
            mainViewModel.updateItem(it)

            println("Updated ${it.name} to ${it.favorite}")
        }
    } else {
        GameGrid(
            navController = navController,
            items = items,
            modifier = Modifier.fillMaxSize()
        ) {
            mainViewModel.updateItem(it)

            println("Updated ${it.name} to ${it.favorite}")
        }
    }

}

