package de.rmrf.partygames.ui.screens.truthordare

import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import de.rmrf.partygames.R
import de.rmrf.partygames.data.entities.TODState
import de.rmrf.partygames.ui.components.CardView
import de.rmrf.partygames.ui.screens.Screen
import de.rmrf.partygames.ui.screens.truthordare.components.TruthOrDareButtons
import de.rmrf.partygames.ui.viewmodel.truthordare.TruthOrDareViewModel
import org.koin.androidx.compose.koinViewModel

@Composable
fun TruthOrDareScreen(
    navController: NavController,
    viewModel: TruthOrDareViewModel = koinViewModel()
) {
    val state = viewModel.state.value

    BackHandler {
        navController.popBackStack(Screen.MainScreen.route, false)
    }

    Column(
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        AnimatedVisibility(visible = state.isInMenu) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.5f),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Text(text = stringResource(R.string.truthordare_please_select), fontSize = 30.sp, textAlign = TextAlign.Center)
            }

        }
        AnimatedVisibility(visible = !state.isInMenu) {
            Box {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .fillMaxHeight(0.5f),
                    verticalArrangement = Arrangement.SpaceAround,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text =
                        if (state.truthOrDareData?.todState?.equals(TODState.DARE) ?: return@AnimatedVisibility) {
                            stringResource(id = R.string.dare)
                        } else {
                            stringResource(id = R.string.truth)
                        }, fontSize = 30.sp, textAlign = TextAlign.Center
                    )
                    CardView(
                        localizedString = state.truthOrDareData.data
                    )
                }
            }
        }
        Spacer(modifier = Modifier.weight(1f))

        TruthOrDareButtons(
            isMenu = state.isInMenu,
            onEvent = {
                viewModel.onEvent(it)
            },
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(0.7f)
        )
    }

}
