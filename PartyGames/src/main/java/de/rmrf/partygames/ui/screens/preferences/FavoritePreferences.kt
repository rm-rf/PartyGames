package de.rmrf.partygames.ui.screens.preferences

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import de.rmrf.partygames.R
import de.rmrf.partygames.preferences.dataStorePreferences
import de.rmrf.partygames.ui.screens.preferences.components.MultiSelectListPreference

@Composable
fun FavoritesPreference() {
    val dataStorePreferences = LocalContext.current.dataStorePreferences
    MultiSelectListPreference(
        title = {
            Text(text = stringResource(id = R.string.favorite_games))
        },
        icon = {}, //This is for proper alingment in the settings screen
        dataStore = dataStorePreferences.favoriteGames,
        entries = stringArrayResource(id = R.array.favorite_game_values),
        entryLabels = stringArrayResource(id = R.array.favorite_game_entries)
    )

}
