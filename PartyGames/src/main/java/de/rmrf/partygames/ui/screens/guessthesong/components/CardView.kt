package de.rmrf.partygames.ui.screens.guessthesong.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Card
import androidx.compose.material3.ShapeDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import de.rmrf.partygames.ui.viewmodel.guessthesong.SongState
import java.util.regex.Pattern

/**
 * Displays a card view with song information.
 *
 * @param song The state of the song.
 * @param modifier The modifier to be applied to the card view.
 * @param width The width of the card view.
 * @param height The height of the card view.
 */

@Composable
fun CardView(
    song: SongState,
    modifier: Modifier = Modifier,
    width: Dp = 360.dp,
    height: Dp = 260.dp
) {
    Box(
        modifier = modifier.size(width, height)
    ) {
        Card(
            modifier = Modifier
                .align(alignment = Alignment.Center)
                .matchParentSize(),
            shape = ShapeDefaults.ExtraLarge
        ) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = modifier.fillMaxSize()
            ) {
                Text(
                    text = when (song.isSongLoading) {
                        true -> "Loading"
                        false -> {
                            when (song.showSolution) {
                                true -> "${
                                    song.videoData?.title?.let {
                                        val regex =
                                            "(\\(Official Video\\))+|(\\(Offizielles Musikvideo\\))+|(\\[Official Music Video])"
                                        val pattern = Pattern.compile(regex)
                                        val matcher = pattern.matcher(it)
                                        matcher.replaceAll("")
                                    }
                                }"

                                false -> "???"
                            }
                        }
                    },
                    fontSize = 24.sp,
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}
