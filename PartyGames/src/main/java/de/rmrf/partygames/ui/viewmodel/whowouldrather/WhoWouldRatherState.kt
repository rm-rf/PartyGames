package de.rmrf.partygames.ui.viewmodel.whowouldrather

import de.rmrf.partygames.data.entities.WhoWouldRatherData

data class WhoWouldRatherState(
    val whoWouldRatherData: WhoWouldRatherData? = null,
    val availableQuestions: Int = 0,
    val isTimerRunning: Boolean = false,
    val isTimerVisible: Boolean = false,
)