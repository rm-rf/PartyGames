package de.rmrf.partygames.ui.screens.whowouldrather

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.People
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.pluralStringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import de.rmrf.partygames.R
import de.rmrf.partygames.ui.components.CardView
import de.rmrf.partygames.ui.components.Countdown
import de.rmrf.partygames.ui.components.GameButtons
import de.rmrf.partygames.ui.screens.Screen
import de.rmrf.partygames.ui.viewmodel.whowouldrather.WhoWouldRatherButtonEvent
import de.rmrf.partygames.ui.viewmodel.whowouldrather.WhoWouldRatherViewModel
import org.koin.androidx.compose.koinViewModel

@Composable
fun WhoWouldRatherScreen(
    navController: NavController,
    viewModel: WhoWouldRatherViewModel = koinViewModel()
) {
    val state = viewModel.state.value

    BackHandler {
        navController.popBackStack(Screen.MainScreen.route, false)
    }

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceEvenly
    ) {

        Box {
            Icon(imageVector = Icons.Outlined.People, contentDescription = null, Modifier.size(180.dp))
        }

        if (state.isTimerVisible) {
            Countdown(
                onTimerStart = {},
                isButtonVisible = false,
                cancelTimer = {
                    viewModel.cancelTimer()
                },
                timeDataState = viewModel.timeData
            )
        } else {
            Box(modifier = Modifier.size(120.dp, 96.dp))
        }

        CardView(question = R.string.whowouldrather_question, localizedString = state.whoWouldRatherData?.whowouldrather ?: return)


        Text(pluralStringResource(id = R.plurals.number_of_tasks, count = state.availableQuestions, state.availableQuestions))

        GameButtons(
            onSkipClick = {
                viewModel.onButtonEvent(event = WhoWouldRatherButtonEvent.SkipButtonClick)
            },
            onDoneClick = {
                viewModel.onButtonEvent(event = WhoWouldRatherButtonEvent.DoneButtonClick)
            }
        )

    }
}
