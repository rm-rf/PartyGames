package de.rmrf.partygames.ui.viewmodel.main

import android.content.Context
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.rmrf.partygames.R
import de.rmrf.partygames.preferences.dataStorePreferences
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class MainViewModel(
    context: Context
) : ViewModel() {

    private val preferenceDataStore = context.dataStorePreferences


    private var _list = mutableStateOf(listOf<Game>())
    val list: State<List<Game>> = _list

    init {
        _list.value = listOf(
            Game.Pantomime(
                name = R.string.name_pantomime,
                imageVector = { Icons.Outlined.TheaterComedy },
                favorite = false
            ),
            Game.SpinTheBottle(
                name = R.string.name_spinthebottle,
                imageVector = { Icons.Outlined.ArrowUpward },
                favorite = false
            ),
            Game.NeverHaveIEver(
                name = R.string.name_neverhaveiever,
                imageVector = { Icons.Outlined.VideogameAsset },
                favorite = false
            ),
            Game.WhoWouldRather(
                name = R.string.name_whowouldrather,
                imageVector = { Icons.Outlined.People },
                favorite = false
            ),
            Game.WouldYouRather(
                name = R.string.name_wouldyourather,
                imageVector = { Icons.Outlined.Balance },
                favorite = false
            ),
            Game.GuessTheSong(
                name = R.string.guessthesong,
                imageVector = { Icons.Outlined.MusicNote },
                favorite = false
            ),
            Game.TruthOrDare(
                name = R.string.name_truthordare,
                imageVector = { Icons.Outlined.TaskAlt },
                favorite = false
            )
        )

        list.value.forEachIndexed { index, game ->
            preferenceDataStore.favoriteGames.getValueFlow().onEach {
                if (it.contains(game.preferencesKey)) {
                    toggleFavorite(index, true)
                }
            }.launchIn(viewModelScope)
        }


    }

    private fun toggleFavorite(index: Int, value: Boolean) {
        val newList = list.value.toMutableList()
        when (val item = newList[index]) {
            is Game.Pantomime -> {
                val newItem = Game.Pantomime(item.name, item.imageVector, value)
                newList[index] = newItem
            }

            is Game.SpinTheBottle -> {
                val newItem = Game.SpinTheBottle(item.name, item.imageVector, value)
                newList[index] = newItem
            }

            is Game.NeverHaveIEver -> {
                val newItem = Game.NeverHaveIEver(item.name, item.imageVector, value)
                newList[index] = newItem
            }

            is Game.WhoWouldRather -> {
                val newItem = Game.WhoWouldRather(item.name, item.imageVector, value)
                newList[index] = newItem
            }

            is Game.WouldYouRather -> {
                val newItem = Game.WouldYouRather(item.name, item.imageVector, value)
                newList[index] = newItem
            }

            is Game.GuessTheSong -> {
                val newItem = Game.GuessTheSong(item.name, item.imageVector, value)
                newList[index] = newItem
            }

            is Game.TruthOrDare -> {
                val newItem = Game.TruthOrDare(item.name, item.imageVector, value)
                newList[index] = newItem
            }
        }
        _list.value = newList
    }


    fun updateItem(game: Game) {
        viewModelScope.launch {
            preferenceDataStore.favoriteGames.saveValue(
                if (game.favorite) {
                    preferenceDataStore.favoriteGames.getValue().minus(game.preferencesKey)
                } else {
                    preferenceDataStore.favoriteGames.getValue().plus(game.preferencesKey)
                }

            )
        }
        toggleFavorite(list.value.indexOf(game), !game.favorite)
    }
}
