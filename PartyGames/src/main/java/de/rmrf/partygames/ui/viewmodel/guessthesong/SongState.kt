package de.rmrf.partygames.ui.viewmodel.guessthesong

import de.rmrf.partygames.data.entities.Playlist
import de.rmrf.partygames.data.entities.SongData
import de.rmrf.partygames.data.entities.api.piped.Video

data class SongState(
    val videoData: Video? = null,
    val songData: SongData? = null,
    val showSongPickerDialog: Boolean = false,
    val showFirstStartDialog: Boolean = false,
    val isSongLoading: Boolean = false,
    val isAddingPlaylist: Boolean = false,
    val showSolution: Boolean = false,
    val availablePlaylist: List<Playlist> = emptyList(),
    val currentPlaylist: Playlist? = null
)
