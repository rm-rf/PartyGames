package de.rmrf.partygames.ui.viewmodel.neverhaveiever

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.rmrf.partygames.data.neverhaveiever.NeverHaveIEverRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NeverHaveIEverViewModel(
    private val repository: NeverHaveIEverRepository
) : ViewModel() {
    private val _state = mutableStateOf<NeverHaveIEverState>(NeverHaveIEverState())
    val state: State<NeverHaveIEverState> = _state

    init {
        getNeverHaveIEver()
    }

    private fun getNeverHaveIEver() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getNeverHaveIEver().also {
                _state.value = state.value.copy(
                    neverHaveIEverData = it
                )
            }
            repository.getAvailableNeverHaveIEverCount().also {
                _state.value = state.value.copy(
                    availableQuestions = it
                )
            }
        }
    }

    fun onButtonEvent(event: NeverHaveIEverButtonEvent) {
        when (event) {
            NeverHaveIEverButtonEvent.DoneButtonClick -> {
                viewModelScope.launch {
                    repository.updateNeverHaveIEver(state.value.neverHaveIEverData ?: return@launch)
                    getNeverHaveIEver()
                }
            }

            NeverHaveIEverButtonEvent.SkipButtonClick -> {
                getNeverHaveIEver()
            }
        }
    }
}
