package de.rmrf.partygames.ui.screens.pantomime.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import de.rmrf.partygames.R
import de.rmrf.partygames.ui.viewmodel.pantomime.PantomimeButtonEvent
import de.rmrf.partygames.ui.viewmodel.pantomime.PantomimeButtonState

@Composable
fun PantomimeButtons(
    modifier: Modifier = Modifier,
    pantomimeButtonState: PantomimeButtonState,
    handleButton: (PantomimeButtonEvent) -> Unit
) {
    Row(
        modifier = modifier.size(280.dp, 60.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        when (pantomimeButtonState) {
            PantomimeButtonState.Nothing -> {

            }

            PantomimeButtonState.DoneVisible -> {
                Button(
                    onClick = {
                        handleButton(PantomimeButtonEvent.DoneButtonClick)
                    }
                ) {
                    Text(stringResource(R.string.all_done))
                }
            }

            PantomimeButtonState.SkipVisible -> {
                Button(
                    onClick = {
                        handleButton(PantomimeButtonEvent.SkipButtonClick)
                    }
                ) {
                    Text(stringResource(R.string.all_skip))
                }
            }

            PantomimeButtonState.TimerFinished -> {
                Button(onClick = {
                    handleButton(PantomimeButtonEvent.YesButtonClick)
                }) {
                    Text(stringResource(R.string.all_yes))
                }
                Button(onClick = {
                    handleButton(PantomimeButtonEvent.NoButtonClick)
                }) {
                    Text(stringResource(R.string.all_no))
                }
            }
        }
    }
}
