package de.rmrf.partygames.ui.screens.about.components

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.ArrowDropDown
import androidx.compose.material.icons.outlined.ArrowDropUp
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CardView(
    title: String,
    text: String,
    isVisible: Boolean,
    modifier: Modifier = Modifier,
    changeVisibility: (Boolean) -> Unit,
) {
    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center
    ) {
        Card(
            onClick = {
                changeVisibility(!isVisible)
            },
            modifier = Modifier.align(alignment = Alignment.Center),
            shape = ShapeDefaults.Medium
        ) {
            Column(
                modifier = modifier
                    .padding(10.dp)
                    .fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = title,
                        fontSize = 30.sp,
                        textAlign = TextAlign.Start,
                    )
                    IconButton(onClick = { changeVisibility(!isVisible) }) {
                        if (!isVisible) {
                            Icon(imageVector = Icons.Outlined.ArrowDropDown, contentDescription = null)
                        } else {
                            Icon(imageVector = Icons.Outlined.ArrowDropUp, contentDescription = null)
                        }
                    }
                }
                AnimatedVisibility(visible = isVisible) {
                    Text(
                        text = text,
                        fontSize = 18.sp,
                        textAlign = TextAlign.Start
                    )
                }
            }
        }
    }
}
