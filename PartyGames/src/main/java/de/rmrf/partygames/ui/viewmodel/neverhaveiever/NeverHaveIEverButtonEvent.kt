package de.rmrf.partygames.ui.viewmodel.neverhaveiever

sealed class NeverHaveIEverButtonEvent {
    data object DoneButtonClick : NeverHaveIEverButtonEvent()

    data object SkipButtonClick : NeverHaveIEverButtonEvent()

}
