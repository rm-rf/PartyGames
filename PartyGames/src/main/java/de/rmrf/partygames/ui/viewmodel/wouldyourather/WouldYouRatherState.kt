package de.rmrf.partygames.ui.viewmodel.wouldyourather

import de.rmrf.partygames.data.entities.WouldYouRatherData

data class WouldYouRatherState(
    val wouldYouRatherData: WouldYouRatherData? = null,
    val availableQuestions: Int = 0,
    val isTimerRunning: Boolean = false,
    val isTimerVisible: Boolean = false,
)
