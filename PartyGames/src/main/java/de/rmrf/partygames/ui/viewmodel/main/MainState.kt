package de.rmrf.partygames.ui.viewmodel.main

data class MainState(
    val list: List<Game> = emptyList()
)
