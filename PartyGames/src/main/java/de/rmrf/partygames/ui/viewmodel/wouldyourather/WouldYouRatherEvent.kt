package de.rmrf.partygames.ui.viewmodel.wouldyourather

sealed class WouldYouRatherEvent {
    data object TimerStart : WouldYouRatherEvent()
    data object TimerFinish : WouldYouRatherEvent()
}
