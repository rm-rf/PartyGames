package de.rmrf.partygames.ui.viewmodel.pantomime

sealed class PantomimeButtonEvent {
    data object SkipButtonClick : PantomimeButtonEvent()
    data object DoneButtonClick : PantomimeButtonEvent()
    data object YesButtonClick : PantomimeButtonEvent()
    data object NoButtonClick : PantomimeButtonEvent()
}
