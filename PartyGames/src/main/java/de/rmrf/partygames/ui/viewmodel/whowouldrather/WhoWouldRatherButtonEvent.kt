package de.rmrf.partygames.ui.viewmodel.whowouldrather

sealed class WhoWouldRatherButtonEvent {
    data object DoneButtonClick : WhoWouldRatherButtonEvent()

    data object SkipButtonClick : WhoWouldRatherButtonEvent()

}