package de.rmrf.partygames.ui.viewmodel.whowouldrather

import android.content.Context
import android.os.CountDownTimer
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import de.rmrf.partygames.data.whowouldrather.WhoWouldRatherRepository
import de.rmrf.partygames.preferences.dataStorePreferences
import de.rmrf.partygames.util.TimerBaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.inject

class WhoWouldRatherViewModel(
    private val repository: WhoWouldRatherRepository
) : TimerBaseViewModel() {

    private val _state = mutableStateOf<WhoWouldRatherState>(WhoWouldRatherState())
    val state: State<WhoWouldRatherState> = _state

    init {
        viewModelScope.launch {
            val timerValue = inject<Context>().value.dataStorePreferences.whoWouldRatherTimerValue.getValue().toLong()
            millisInFuture =
                if (timerValue >= 5) {
                    (timerValue + 1) * 1000
                } else {
                    inject<Context>().value.dataStorePreferences.whoWouldRatherTimerValue.defaultValue.toLong()
                }

            timeData.value = millisInFuture
            _state.value = state.value.copy(
                isTimerVisible = inject<Context>().value.dataStorePreferences.enableWhoWouldRatherTimer.getValue()
            )
        }
        getWhoWouldRather()
    }

    private fun getWhoWouldRather() {
        if (state.value.isTimerVisible) {
            startTimer()
        }
        viewModelScope.launch(Dispatchers.IO) {
            repository.getWhoWouldRather().also {
                _state.value = state.value.copy(
                    whoWouldRatherData = it,
                    isTimerRunning = false
                )
            }
            repository.getAvailableWhoWouldRatherCount().also {
                _state.value = state.value.copy(
                    availableQuestions = it
                )
            }
        }
    }

    fun onButtonEvent(event: WhoWouldRatherButtonEvent) {
        when (event) {
            WhoWouldRatherButtonEvent.DoneButtonClick -> {
                viewModelScope.launch {
                    repository.updateWhoWouldRather(state.value.whoWouldRatherData ?: return@launch)
                    cancelTimer()
                    getWhoWouldRather()
                }
            }

            WhoWouldRatherButtonEvent.SkipButtonClick -> {
                cancelTimer()
                getWhoWouldRather()
            }
        }
    }

    fun onEvent(event: WhoWouldRatherEvent) {
        when (event) {
            WhoWouldRatherEvent.TimerFinish -> {
                _state.value = state.value.copy(
                    isTimerRunning = false
                )
                getWhoWouldRather()
            }

            WhoWouldRatherEvent.TimerStart -> {
                _state.value = state.value.copy(
                    isTimerRunning = true
                )
                startTimer()
            }
        }
    }

    override fun createTimerObject(): CountDownTimer {
        return object : CountDownTimer(millisInFuture, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timeData.value = millisUntilFinished
            }

            override fun onFinish() {
                onEvent(WhoWouldRatherEvent.TimerFinish)
                running.value = false
            }
        }
    }
}
