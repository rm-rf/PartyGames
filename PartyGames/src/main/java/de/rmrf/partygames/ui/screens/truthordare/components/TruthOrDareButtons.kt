package de.rmrf.partygames.ui.screens.truthordare.components

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import de.rmrf.partygames.R
import de.rmrf.partygames.ui.viewmodel.truthordare.TruthOrDareEvent

@Composable
fun TruthOrDareButtons(
    isMenu: Boolean,
    onEvent: (TruthOrDareEvent) -> Unit,
    modifier: Modifier = Modifier
) {
    Box(modifier = modifier.fillMaxWidth()) {
        AnimatedVisibility(visible = isMenu, enter = fadeIn(), exit = fadeOut()) {
            Row(
                modifier.matchParentSize(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                Button(
                    onClick = {
                        onEvent(TruthOrDareEvent.MenuEvent.DareButtonClick)
                    }
                ) {
                    Text(text = stringResource(id = R.string.dare))
                }
                Button(
                    onClick = {
                        onEvent(TruthOrDareEvent.MenuEvent.RandomButtonClick)
                    }
                ) {
                    Text(text = stringResource(id = R.string.all_random))
                }
                Button(
                    onClick = {
                        onEvent(TruthOrDareEvent.MenuEvent.TruthButtonClick)
                    }
                ) {
                    Text(text = stringResource(id = R.string.truth))
                }
            }
        }
        AnimatedVisibility(visible = !isMenu, enter = fadeIn(), exit = fadeOut()) {
            Row(
                modifier.matchParentSize(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceEvenly
            ) {
                Button(
                    onClick = {
                        onEvent(TruthOrDareEvent.InGameEvent.SkipButtonClick)
                    }
                ) {
                    Text(text = stringResource(id = R.string.all_skip))
                }
                Button(
                    onClick = {
                        onEvent(TruthOrDareEvent.InGameEvent.DoneButtonClick)
                    }
                ) {
                    Text(text = stringResource(id = R.string.all_done))
                }
            }
        }
    }

}
