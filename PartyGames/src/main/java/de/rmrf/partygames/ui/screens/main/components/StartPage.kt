package de.rmrf.partygames.ui.screens.main.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Star
import androidx.compose.material.icons.outlined.StarOutline
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import de.rmrf.partygames.R
import de.rmrf.partygames.ui.screens.Screen
import de.rmrf.partygames.ui.viewmodel.main.Game

@Composable
fun StartPageHeader() {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_launcher_foreground),
            contentDescription = null,
            modifier = Modifier
                .size(
                    width = 160.dp,
                    height = 160.dp
                )
        )
        Text(
            text = stringResource(id = R.string.main_select_game),
            fontSize = 24.sp,
            modifier = Modifier.padding(top = 10.dp, bottom = 6.dp)
        )
    }
}

internal fun handleNavigation(
    navController: NavController,
    game: Game
) {
    navController.navigate(
        route = when (game) {
            is Game.Pantomime -> Screen.PantomimeScreen.route
            is Game.SpinTheBottle -> Screen.SpinTheBottleScreen.route
            is Game.NeverHaveIEver -> Screen.NeverHaveIEverScreen.route
            is Game.WhoWouldRather -> Screen.WhoWouldRatherScreen.route
            is Game.WouldYouRather -> Screen.WouldYouRatherScreen.route
            is Game.GuessTheSong -> Screen.GuessTheSongScreen.route
            is Game.TruthOrDare -> Screen.TruthOrDareScreen.route
        }
    )
}

@Composable
internal fun FavoriteButton(
    updateFavorite: (Game) -> Unit,
    game: Game
) {
    IconButton(onClick = {
        updateFavorite(game)
    }) {
        Icon(
            imageVector = if (game.favorite) {
                Icons.Outlined.Star
            } else {
                Icons.Outlined.StarOutline
            },
            contentDescription = null
        )
    }
}
