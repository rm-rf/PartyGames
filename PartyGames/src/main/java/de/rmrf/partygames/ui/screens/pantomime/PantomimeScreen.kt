package de.rmrf.partygames.ui.screens.pantomime

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.TheaterComedy
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.pluralStringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import de.rmrf.partygames.R
import de.rmrf.partygames.ui.components.CardView
import de.rmrf.partygames.ui.components.Countdown
import de.rmrf.partygames.ui.screens.Screen
import de.rmrf.partygames.ui.screens.pantomime.components.PantomimeButtons
import de.rmrf.partygames.ui.viewmodel.pantomime.PantomimeEvent
import de.rmrf.partygames.ui.viewmodel.pantomime.PantomimeViewModel
import org.koin.androidx.compose.koinViewModel


//TODO: Do not reset timer on done if timer is running. This has to be implemented for the gamemode with points
@Composable
fun PantomimeScreen(
    navController: NavController,
    viewModel: PantomimeViewModel = koinViewModel()
) {
    val state = viewModel.state.value

    BackHandler {
        navController.popBackStack(Screen.MainScreen.route, false)
    }

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceEvenly
    ) {

        Box {
            Icon(
                Icons.Outlined.TheaterComedy,
                null,
                modifier = Modifier.size(180.dp)
            )
        }

        Countdown(
            onTimerStart = {
                viewModel.onEvent(PantomimeEvent.TimerStart)
            },
            isButtonVisible = state.isTimerButtonShown,
            cancelTimer = {
                viewModel.cancelTimer()
            },
            timeDataState = viewModel.timeData
        )

        
        CardView(
            question = R.string.game_pantomime_try_to,
            localizedString = state.pantomimeData?.pantomime ?: return,
            enableClick = true,
            onCardClick = {
                viewModel.onEvent(PantomimeEvent.CardClick)
            },
            notVisibleText = "???",
            isVisible = state.isTaskShown

        )

        Text(pluralStringResource(id = R.plurals.number_of_tasks, count = state.availableQuestions, state.availableQuestions))

        PantomimeButtons(
            pantomimeButtonState = state.buttonState
        ) {
            viewModel.onButtonEvent(it)
        }
    }


}
