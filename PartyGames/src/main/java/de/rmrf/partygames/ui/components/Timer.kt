package de.rmrf.partygames.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.PlayCircle
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import de.rmrf.partygames.util.formatTime

/**
 * A composable function that displays a countdown timer.
 *
 * @param modifier The modifier for styling the countdown timer.
 * @param onTimerStart A callback function triggered when the timer starts.
 * @param cancelTimer A callback function triggered when the timer is canceled.
 * @param isButtonVisible Boolean indicating whether to show the play button.
 * @param timeDataState The state containing the time data for the countdown timer.
 */
@Composable
fun Countdown(
    modifier: Modifier = Modifier,
    onTimerStart: () -> Unit,
    cancelTimer: () -> Unit,
    isButtonVisible: Boolean,
    timeDataState: State<Long>
) {
    val timeData by timeDataState

    LaunchedEffect(true) {
        timeDataState.asLongState()
    }

    DisposableEffect(key1 = "key") {
        onDispose {
            cancelTimer()
        }
    }
    Box(
        modifier = modifier.size(120.dp, 76.dp),
        contentAlignment = Alignment.TopCenter
    ) {
        Column(
            modifier.matchParentSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = timeData.formatTime(),
                fontSize = 24.sp
            )
            if (isButtonVisible) {
                IconButton(
                    onClick = {
                        onTimerStart()
                    }
                ) {
                    Icon(Icons.Outlined.PlayCircle, null, modifier.size(48.dp))
                }
            }
        }
    }
}
