package de.rmrf.partygames.ui.viewmodel.pantomime

sealed class PantomimeButtonState {
    data object Nothing : PantomimeButtonState()
    data object SkipVisible : PantomimeButtonState()
    data object DoneVisible : PantomimeButtonState()
    data object TimerFinished : PantomimeButtonState()
}
