package de.rmrf.partygames.ui.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp

/**
 * Adds vertical space to the layout.
 *
 * @param dp The size of the vertical space in density-independent pixels.
 */
@Composable
fun Spacer(
    dp: Dp
) {
    Spacer(modifier = Modifier.height(dp))
}
