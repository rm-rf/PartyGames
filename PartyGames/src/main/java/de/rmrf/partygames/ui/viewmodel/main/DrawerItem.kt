package de.rmrf.partygames.ui.viewmodel.main

import androidx.compose.ui.graphics.vector.ImageVector

open class NavItem(
    open val id: Int,
    open val icon: ImageVector,
    open val label: String
)

data class NavItemSettings(
    override val id: Int,
    override val icon: ImageVector,
    override val label: String
) : NavItem(id, icon, label)
