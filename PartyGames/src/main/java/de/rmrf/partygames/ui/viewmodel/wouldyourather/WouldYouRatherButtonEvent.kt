package de.rmrf.partygames.ui.viewmodel.wouldyourather

sealed class WouldYouRatherButtonEvent {
    data object SkipButtonClick : WouldYouRatherButtonEvent()
    data object DoneButtonClick : WouldYouRatherButtonEvent()
}