package de.rmrf.partygames.ui.screens.preferences.components

import androidx.compose.material3.Switch
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import de.rmrf.partygames.preferences.PartyGamesPreferenceDataStore
import kotlinx.coroutines.launch

@Composable
fun SwitchPreference(
    title: (@Composable () -> Unit),
    summary: (@Composable () -> Unit)? = null,
    icon: (@Composable () -> Unit)? = null,
    onCheckedChange: ((checked: Boolean) -> Boolean)? = null,
    dependency: PartyGamesPreferenceDataStore<*>? = null,
    dataStore: PartyGamesPreferenceDataStore<Boolean>
) {
    val scope = rememberCoroutineScope()

    Preference(
        title = title,
        summary = summary,
        icon = icon,
        dependency = dependency,
        dataStore = dataStore,
        onClick = { value ->
            scope.launch { dataStore.saveValue(onCheckedChange?.invoke(!value) ?: !value) }
        },
        trailingContent = { value, enabled ->
            Switch(
                checked = value,
                onCheckedChange = { newValue ->
                    scope.launch { dataStore.saveValue(onCheckedChange?.invoke(newValue) ?: newValue) }
                },
                enabled = enabled
            )
        }
    )
}
