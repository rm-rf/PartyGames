package de.rmrf.partygames.ui.screens.neverhaveiever

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.VideogameAsset
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.pluralStringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import de.rmrf.partygames.R
import de.rmrf.partygames.ui.components.CardView
import de.rmrf.partygames.ui.components.GameButtons
import de.rmrf.partygames.ui.screens.Screen
import de.rmrf.partygames.ui.viewmodel.neverhaveiever.NeverHaveIEverButtonEvent
import de.rmrf.partygames.ui.viewmodel.neverhaveiever.NeverHaveIEverViewModel
import org.koin.androidx.compose.koinViewModel

@Composable
fun NeverHaveIEverScreen(
    navController: NavController,
    viewModel: NeverHaveIEverViewModel = koinViewModel()
) {
    val state = viewModel.state.value

    BackHandler {
        navController.popBackStack(Screen.MainScreen.route, false)
    }

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.SpaceEvenly
    ) {

        Box {
            Icon(imageVector = Icons.Outlined.VideogameAsset, contentDescription = null, Modifier.size(180.dp))
        }

        CardView(question = R.string.name_neverhaveiever, localizedString = state.neverHaveIEverData?.neverhaveiever ?: return)

        Text(pluralStringResource(id = R.plurals.number_of_tasks, count = state.availableQuestions, state.availableQuestions))

        GameButtons(
            onSkipClick = {
                viewModel.onButtonEvent(event = NeverHaveIEverButtonEvent.SkipButtonClick)
            },
            onDoneClick = {
                viewModel.onButtonEvent(event = NeverHaveIEverButtonEvent.DoneButtonClick)
            }
        )

    }
}
