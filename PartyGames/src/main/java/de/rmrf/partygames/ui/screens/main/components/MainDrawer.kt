package de.rmrf.partygames.ui.screens.main.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.Feed
import androidx.compose.material.icons.outlined.Extension
import androidx.compose.material.icons.outlined.People
import androidx.compose.material.icons.outlined.Settings
import androidx.compose.material.icons.outlined.Translate
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import de.rmrf.partygames.R
import de.rmrf.partygames.ui.viewmodel.main.NavItem
import de.rmrf.partygames.ui.viewmodel.main.NavItemSettings

@Composable
fun DrawerItems(
    onItemClick: (item: NavItem) -> Unit
) {
    val drawerItems = listOf(
        NavItemSettings(
            id = 2,
            icon = Icons.Outlined.Settings,
            label = stringResource(id = R.string.settings)
        ),
        NavItemSettings(
            id = 3,
            icon = Icons.Outlined.People,
            label = stringResource(id = R.string.players)
        ),
        NavItemSettings(
            id = 4,
            icon = Icons.AutoMirrored.Outlined.Feed,
            label = stringResource(id = R.string.about)
        ),
        NavItemSettings(
            id = 5,
            icon = Icons.Outlined.Extension,
            label = stringResource(id = R.string.all_third_party_libraries)
        ),
        NavItemSettings(
            id = 6,
            icon = Icons.Outlined.Translate,
            label = stringResource(id = R.string.all_translate)
        ),
    )

    Spacer(modifier = Modifier.height(24.dp))
    DrawerImage(painterResource(R.drawable.ic_launcher_foreground))
    DrawerText(stringResource(R.string.app_name), MaterialTheme.typography.headlineSmall)
    DrawerText(text = stringResource(id = R.string.preference_all_title))

    drawerItems.forEach { item ->
        NavigationDrawerItem(
            label = {
                Text(text = item.label)
            },
            selected = false,
            icon = {
                Icon(
                    imageVector = item.icon,
                    contentDescription = "Open ${item.label}"
                )
            },
            onClick = {
                onItemClick(item)
            },
            modifier = Modifier.padding(NavigationDrawerItemDefaults.ItemPadding)
        )
    }

}


@Composable
fun DrawerDivider() {
    HorizontalDivider(
        modifier = Modifier.padding(vertical = 8.dp),
        color = MaterialTheme.colorScheme.outline
    )
}

@Composable
fun DrawerText(text: String) {
    Text(
        text = text,
        style = MaterialTheme.typography.labelMedium,
        modifier = Modifier.padding(start = 28.dp, top = 16.dp, bottom = 8.dp)
    )
}

@Composable
fun DrawerText(text: String, style: TextStyle) {
    Text(
        text = text,
        style = style,
        modifier = Modifier.padding(start = 28.dp, top = 16.dp, bottom = 8.dp)
    )
}

@Composable
fun DrawerImage(painter: Painter) {
    Image(
        painter = painter,
        contentDescription = null,
        modifier = Modifier
            .padding(start = 10.dp)
            .size(144.dp)
    )
}
