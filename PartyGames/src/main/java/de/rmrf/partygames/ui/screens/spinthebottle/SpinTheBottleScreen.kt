package de.rmrf.partygames.ui.screens.spinthebottle

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Straight
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.IconButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.navigation.NavController
import de.rmrf.partygames.ui.screens.Screen
import de.rmrf.partygames.ui.screens.spinthebottle.components.RotateableSurface
import kotlin.random.Random

@Composable
fun SpinTheBottleScreen(
    navController: NavController
) {

    BackHandler {
        navController.popBackStack(Screen.MainScreen.route, false)
    }

    var rotation by remember {
        mutableFloatStateOf(0F)
    }

    Box(
        modifier = Modifier.fillMaxSize(),
    ) {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            RotateableSurface(
                rotation = rotation
            ) {
                IconButton(
                    modifier = Modifier.fillMaxSize(),
                    onClick = {
                        rotation = generateRotationVelocity()
                    },
                    colors = IconButtonDefaults.iconButtonColors(contentColor = MaterialTheme.colorScheme.onSurface)
                ) {
                    Icon(
                        Icons.Outlined.Straight,
                        null,
                        modifier = it.fillMaxSize(),
                    )
                }
            }
        }
    }
}

fun generateRotationVelocity(): Float {
    return Random.nextFloat() * 720.0f - 360.0f
}
