package de.rmrf.partygames.ui.viewmodel.whowouldrather

sealed class WhoWouldRatherEvent {
    data object TimerStart : WhoWouldRatherEvent()
    data object TimerFinish : WhoWouldRatherEvent()
}
