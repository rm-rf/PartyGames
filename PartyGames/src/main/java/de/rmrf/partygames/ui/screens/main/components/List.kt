package de.rmrf.partygames.ui.screens.main.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import de.rmrf.partygames.ui.viewmodel.main.Game

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun GameList(
    navController: NavController,
    items: List<Game>,
    modifier: Modifier = Modifier,
    updateFavorite: (Game) -> Unit
) {
    LazyColumn(
        contentPadding = PaddingValues(4.dp),
        modifier = modifier.fillMaxSize()
    ) {
        item {
            StartPageHeader()
        }
        items(items) {
            OutlinedCard(
                onClick = {
                    handleNavigation(navController, it)
                },
                elevation = CardDefaults.cardElevation(1.dp),
                modifier = Modifier
                    .fillMaxWidth(),
            ) {
                Row(
                    modifier = Modifier.fillParentMaxWidth().padding(all = 8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    FavoriteButton(updateFavorite = updateFavorite, game = it)
                    Text(
                        stringResource(id = it.name),
                        fontSize = 24.sp,
                        textAlign = TextAlign.Start,
                        modifier = Modifier.weight(1f)
                    )
                    Box {
                        Icon(imageVector = it.imageVector(), null)
                    }
                }
            }
            Spacer(modifier = Modifier.height(8.dp))
        }
    }
}
