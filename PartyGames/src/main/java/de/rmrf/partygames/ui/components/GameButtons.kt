package de.rmrf.partygames.ui.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import de.rmrf.partygames.R

@Composable
fun GameButtons(
    modifier: Modifier = Modifier,
    onSkipClick: () -> Unit,
    onDoneClick: () -> Unit,
) {
    Row(
        modifier = modifier.size(280.dp, 60.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        Button(
            onClick = onSkipClick
        ) {
            Text(stringResource(R.string.all_skip))
        }
        Button(
            onClick = onDoneClick
        ) {
            Text(stringResource(R.string.all_done))
        }
    }

}
