package de.rmrf.partygames.ui.viewmodel.truthordare

sealed class TruthOrDareEvent {

    sealed class InGameEvent : TruthOrDareEvent() {
        data object SkipButtonClick : InGameEvent()
        data object DoneButtonClick : InGameEvent()
    }

    sealed class MenuEvent : TruthOrDareEvent() {
        data object TruthButtonClick : MenuEvent()
        data object DareButtonClick : MenuEvent()
        data object RandomButtonClick : MenuEvent()
    }

}
