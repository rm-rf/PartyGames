package de.rmrf.partygames.ui.screens.spinthebottle.components


import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.AnimationEndReason
import androidx.compose.animation.splineBasedDecay
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.unit.Density

@Composable
fun RotateableSurface(
    rotation: Float,
    onFinish: (() -> Unit)? = null,
    content: @Composable (modifier: Modifier) -> Unit
) {
    val animatableRotation = remember { Animatable(0F) }
    LaunchedEffect(key1 = rotation) {
        if (animatableRotation.animateDecay(
            rotation * 5,
            splineBasedDecay(density = Density(0.1F))
        ).endReason == AnimationEndReason.Finished) {
            if (animatableRotation.value != 0F) {
                onFinish?.invoke()
            }
        }
    }
    content(Modifier.rotate(animatableRotation.value))
}
