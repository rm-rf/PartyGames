package de.rmrf.partygames.ui.viewmodel.neverhaveiever

import de.rmrf.partygames.data.entities.NeverHaveIEverData

data class NeverHaveIEverState(
    val neverHaveIEverData: NeverHaveIEverData? = null,
    val availableQuestions: Int = 0
)
