package de.rmrf.partygames.ui.screens.preferences

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.imePadding
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.ViewList
import androidx.compose.material.icons.outlined.*
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavController
import de.rmrf.partygames.R
import de.rmrf.partygames.preferences.*
import de.rmrf.partygames.ui.screens.Screen
import de.rmrf.partygames.ui.screens.preferences.components.*
import de.rmrf.partygames.util.getLocale

@Composable
fun PreferencesMainScreen(
    navController: NavController,
) {
    VerticalScrollColumn {

        PreferenceScreen(
            key = Screen.PreferencesScreen.PreferencesScreenGames.route,
            icon = {
                Icon(imageVector = Icons.Outlined.Games, contentDescription = null)
            },
            title = {
                Text(text = stringResource(id = R.string.preference_screen_games))
            }, navController = navController
        )

        PreferenceScreen(
            key = Screen.PreferencesScreen.PreferencesUIScreen.route,
            icon = {
                Icon(imageVector = Icons.Outlined.Style, null)
            },
            title = {
                Text(text = "UI")
            }, navController = navController
        )

        PreferenceCategory(title = stringResource(id = R.string.preference_category_other)) {
            val dataStorePreferences = LocalContext.current.dataStorePreferences
            MultiSelectListPreference(
                title = {
                    Text(text = stringResource(id = R.string.favorite_games))
                },
                icon = {
                    Icon(imageVector = Icons.Outlined.StarOutline, contentDescription = null)
                },
                dataStore = dataStorePreferences.favoriteGames,
                entries = stringArrayResource(id = R.array.favorite_game_values),
                entryLabels = stringArrayResource(id = R.array.favorite_game_entries)
            )

            SwitchPreference(
                title = { Text(stringResource(R.string.settings_show_startup_dialog)) },
                summary = { Text(stringResource(R.string.settings_affects_next_app_start)) },
                dataStore = dataStorePreferences.firstStart
            )
            SmallPreferenceCategory(title = stringResource(id = R.string.app_name)) {
                Preference(
                    title = { Text(text = stringResource(id = R.string.copyright_rmrf)) },
                    dataStore = PartyGamesPreferenceDataStore.emptyDataStore()
                )
            }
        }
    }
}

@Composable
fun PreferencesGameScreen() {
    val dataStorePreferences = LocalContext.current.dataStorePreferences
    VerticalScrollColumn {
        PreferenceCategory(title = stringResource(id = R.string.preference_pantomime_title)) {
            SwitchPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_pantomime_score))
                },
                summary = {
                    Text(text = stringResource(id = R.string.preference_pantomime_score_summary))
                },
                dataStore = dataStorePreferences.enablePantomimeScore,
            )
            SwitchPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_pantomime_partner_title))
                },
                summary = {
                    Text(text = stringResource(id = R.string.preference_pantomime_partner_summary))
                },
                dataStore = dataStorePreferences.enablePantomimePartnerMode
            )
            SwitchPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_pantomime_skipped_tasks_title))
                },
                summary = {
                    Text(text = stringResource(id = R.string.preference_pantomime_skipped_tasks_summary))
                },
                dataStore = dataStorePreferences.enableOversteppedPantomimeCountsAsUsed
            )
            NumericInputPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_all_choose_time))
                },
                unit = "sec",
                dataStore = dataStorePreferences.pantomimeTimerValue
            )
        }
        PreferenceCategory(title = stringResource(id = R.string.preference_wouldyourather_title)) {
            SwitchPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_wouldyourather_18))
                },
                dataStore = dataStorePreferences.enableWouldYouRather18Mode
            )
            SwitchPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_wouldyourather_timer))
                },
                dataStore = dataStorePreferences.enableWouldYouRatherTimer
            )
            NumericInputPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_all_choose_time))
                },
                unit = "sec",
                dependency = dataStorePreferences.enableWouldYouRatherTimer,
                dataStore = dataStorePreferences.wouldYouRatherTimerValue
            )
        }
        PreferenceCategory(title = stringResource(id = R.string.preference_whowouldrather_title)) {
            SwitchPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_whowouldrather_18))
                },
                dataStore = dataStorePreferences.enableWhoWouldRather18Mode
            )
            SwitchPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_whowouldrather_timer))
                },
                dataStore = dataStorePreferences.enableWhoWouldRatherTimer
            )
            NumericInputPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_all_choose_time))
                },
                unit = "sec",
                dependency = dataStorePreferences.enableWhoWouldRatherTimer,
                dataStore = dataStorePreferences.whoWouldRatherTimerValue
            )
        }
        PreferenceCategory(title = stringResource(id = R.string.preference_truthordare_title)) {
            SwitchPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_truthordare_18))
                },
                dataStore = dataStorePreferences.enableTruthOrDare18Mode
            )
            SwitchPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_truthordare_timer))
                },
                dataStore = dataStorePreferences.enableTruthOrDareTimer
            )
            NumericInputPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_all_choose_time))
                },
                unit = "sec",
                dependency = dataStorePreferences.enableTruthOrDareTimer,
                dataStore = dataStorePreferences.truthOrDareTimerValue
            )
        }
        PreferenceCategory(title = stringResource(id = R.string.preference_neverhaveiever_title)) {
            SwitchPreference(
                title = {
                    Text(text = stringResource(id = R.string.preference_neverhaveiever_18))
                },
                dataStore = dataStorePreferences.enableNeverHaveIEver18Mode
            )
        }
    }
}


@Composable
fun PreferencesUIScreen() {
    val localeSettingsLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) {}
    val dataStorePreferences = LocalContext.current.dataStorePreferences
    val packageName = LocalContext.current.packageName
    VerticalScrollColumn {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            Preference(
                title = { Text(text = stringResource(R.string.preference_app_language)) },
                summary = { Text(text = getLocale().displayLanguage) },
                icon = { Icon(imageVector = Icons.Outlined.Translate, contentDescription = null)},
                dataStore = PartyGamesPreferenceDataStore.emptyDataStore(),
                onClick = {
                    localeSettingsLauncher.launch(Intent(Settings.ACTION_APP_LOCALE_SETTINGS, Uri.parse("package:${packageName}")))
                }
            )
        }
        SwitchPreference(
            title = { stringResource(R.string.settings_enable_simple_main) },
            icon = {
            Icon(
                imageVector = if (dataStorePreferences.enableSimpleMainScreen.getState().value) {
                    Icons.AutoMirrored.Outlined.ViewList
                } else {
                    Icons.Outlined.GridView
                },
                null
            )
            },
            dataStore = dataStorePreferences.enableSimpleMainScreen
        )
        SmallPreferenceCategory(title = stringResource(id = R.string.preference_category_themes)) {
            ColorPreference(
                title = { Text(stringResource(id = R.string.preference_theme_color)) },
                icon = {
                    Icon(
                        imageVector = Icons.Outlined.FormatPaint,
                        contentDescription = null
                    )
                },
                dataStore = dataStorePreferences.themeColor,
                defaultValueLabel = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
                    stringResource(id = R.string.preference_theme_color_default_label)
                else
                    null
            )
            ListPreference(
                title = { Text(stringResource(id = R.string.preference_dark_theme)) },
                summary = { Text(it.second) },
                icon = {
                    Icon(
                        imageVector = Icons.Outlined.BrightnessMedium,
                        contentDescription = null
                    )
                },
                entries = stringArrayResource(id = R.array.preference_dark_theme_values),
                entryLabels = stringArrayResource(id = R.array.preference_dark_theme),
                dataStore = dataStorePreferences.darkTheme
            )

            SwitchPreference(
                title = { Text(stringResource(id = R.string.preference_dark_theme_oled)) },
                summary = { Text(stringResource(id = R.string.preference_dark_theme_oled_desc)) },
                icon = {
                    Icon(
                        imageVector = Icons.Outlined.InvertColors,
                        contentDescription = null
                    )
                },
                dependency = dataStorePreferences.darkTheme,
                dataStore = dataStorePreferences.darkThemeOled
            )
        }
    }
}

@Composable
fun VerticalScrollColumn(content: @Composable ColumnScope.() -> Unit) {
    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .bottomInsets(), content = content
    )
}

@Composable
fun Modifier.bottomInsets() = this.then(
    Modifier
        .navigationBarsPadding()
        .imePadding()
)
