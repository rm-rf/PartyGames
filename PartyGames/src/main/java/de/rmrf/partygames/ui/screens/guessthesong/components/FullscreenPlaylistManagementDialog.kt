package de.rmrf.partygames.ui.screens.guessthesong.components

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.LocalIndication
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import de.rmrf.partygames.R
import de.rmrf.partygames.data.entities.Playlist
import de.rmrf.partygames.ui.components.AppScaffold
import de.rmrf.partygames.ui.components.DynamicHeightAlertDialog
import de.rmrf.partygames.ui.components.disabled
import de.rmrf.partygames.ui.viewmodel.guessthesong.GuessTheSongEvent
import de.rmrf.partygames.util.urlParser

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FullscreenPlaylistManagementDialog(
    title: @Composable () -> Unit,
    isLoading: Boolean,
    onEvent: (event: GuessTheSongEvent) -> Unit,
    currentSelectedPlaylist: Playlist? = null,
    onDismiss: (success: Boolean) -> Unit = {},
    playlists: List<Playlist> = emptyList()
) {
    BackHandler {
        onDismiss(false)
    }

    val currentlySelectedItem = remember {
        mutableStateOf(currentSelectedPlaylist)
    }

    AppScaffold(
        topBar = {
            CenterAlignedTopAppBar(
                title = title,
                navigationIcon = {
                    IconButton(onClick = { onDismiss(false) }) {
                        Icon(
                            imageVector = Icons.Outlined.Close,
                            contentDescription = null
                        )
                    }
                },
                actions = {
                    if (currentlySelectedItem.value != null) {
                        IconButton(
                            onClick = {
                                onDismiss(true)
                                onEvent(GuessTheSongEvent.DialogEvent.SelectPlaylist(playlist = currentlySelectedItem.value ?: return@IconButton))
                            }
                        ) {
                            Icon(imageVector = Icons.Outlined.Check, contentDescription = null)
                        }
                    } else if (isLoading) {
                        CircularProgressIndicator()
                    }
                }
            )
        }

    ) { innerPadding ->
        Column(
            modifier = Modifier
                .padding(innerPadding)
                .fillMaxSize(),
            verticalArrangement = Arrangement.SpaceAround
        ) {
            PlaylistElements(
                playlists = playlists,
                onSelectChange = {
                    currentlySelectedItem.value = it
                },
                provideSelected = {
                    currentlySelectedItem.value
                },
                isLoading = isLoading,
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                deletedEvent = {
                    onEvent(it)
                }
            )
            HorizontalDivider()
            de.rmrf.partygames.ui.components.Spacer(8.dp)
            AddPlaylistItem(
                addURI = { playlistId ->
                    onEvent(GuessTheSongEvent.DialogEvent.AddPlaylist(playlistId = playlistId))
                },
                isLoading = isLoading
            )
            de.rmrf.partygames.ui.components.Spacer(24.dp)
        }
    }
}

@Composable
fun PlaylistElements(
    playlists: List<Playlist>,
    onSelectChange: (selectedItem: Playlist) -> Unit,
    provideSelected: () -> Playlist?,
    deletedEvent: (event: GuessTheSongEvent) -> Unit,
    isLoading: Boolean,
    modifier: Modifier = Modifier,
    contentPadding: PaddingValues = PaddingValues(0.dp)
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
    ) {
        LazyColumn(
            modifier = Modifier.fillMaxHeight(),
            contentPadding = contentPadding
        ) {
            items(playlists) { playlist ->
                val interactionSource = remember {
                    MutableInteractionSource()
                }
                val deleteDialog = remember {
                    mutableStateOf(false)
                }
                ConfirmDeleteDialog(
                    emitDelete = { delete ->
                        deleteDialog.value = false
                        if (delete) {
                            deletedEvent(GuessTheSongEvent.DialogEvent.DeletePlaylist(playlist = playlist))
                        }
                    },
                    playlist = playlist,
                    isVisible = deleteDialog.value
                )


                Row(
                    modifier = Modifier.fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    RadioButton(
                        selected = provideSelected() == playlist,
                        onClick = {
                            onSelectChange(playlist)
                        },
                        enabled = !isLoading
                    )
                    Text(
                        text = "${playlist.playlistName} (${playlist.videoCount})",
                        style = MaterialTheme.typography.bodyLarge,
                        modifier = Modifier
                            .clickable(
                                interactionSource = interactionSource,
                                indication = LocalIndication.current,
                                role = Role.RadioButton,
                                enabled = !isLoading
                            ) {
                                onSelectChange(playlist)
                            }
                            .padding(
                                vertical = 16.dp,
                            )
                            .weight(1f)

                    )
                    IconButton(
                        onClick = {
                            deleteDialog.value = true
                        }
                    ) {
                        Icon(Icons.Outlined.Delete, null)
                    }
                }
            }
            if (playlists.isEmpty()) {
                item {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(50.dp),
                        contentAlignment = Alignment.Center
                    ) {
                        Text(
                            modifier = Modifier
                                .fillParentMaxWidth()
                                .disabled(true),
                            text = stringResource(R.string.guessthesong_add_playlist),
                            textAlign = TextAlign.Center,
                            style = MaterialTheme.typography.headlineSmall
                        )
                    }
                }
            }
        }
    }
}


@Composable
fun ConfirmDeleteDialog(
    emitDelete: (Boolean) -> Unit,
    playlist: Playlist,
    isVisible: Boolean
) {
    if (isVisible){
        DynamicHeightAlertDialog(
            onDismissRequest = {
                emitDelete(false)
            },
            title = {
                Text("Delete: \"${playlist.playlistName}\"")
            },
            text = {
                Text(stringResource(R.string.guessthesong_delete_playlist_text))
            },
            confirmButton = {
                TextButton(
                    onClick = {
                        emitDelete(true)
                    }
                ) {
                    Text(stringResource(R.string.all_delete))
                }
            },
            dismissButton = {
                TextButton(
                    onClick = {
                        emitDelete(false)
                    }
                ) {
                    Text(stringResource(R.string.all_cancel))
                }
            }
        )
    }

}

@Composable
fun AddPlaylistItem(
    modifier: Modifier = Modifier,
    isLoading: Boolean,
    addURI: (text: String) -> Unit,
) {
    val playlistUrl = remember { mutableStateOf("") }
    Box(modifier = modifier, contentAlignment = Alignment.Center) {
        OutlinedTextField(
            value = playlistUrl.value,
            enabled = !isLoading,
            label = {
                Text(stringResource(R.string.guessthesong_enter_url))
            },
            placeholder = {
                Text("https://youtube.com/playlist?list=")
            },
            isError = (playlistUrl.value.urlParser() == null) && playlistUrl.value.isNotEmpty(),
            trailingIcon = {
                if ((playlistUrl.value.urlParser() == null)) {
                    IconButton(
                        onClick = {
                            playlistUrl.value = ""
                        }
                    ) {
                        Icon(Icons.Outlined.RemoveCircleOutline, null)
                    }
                } else {
                    IconButton(
                        onClick = {
                            addURI(playlistUrl.value.urlParser() ?: return@IconButton)
                            playlistUrl.value = ""
                        }
                    ) {
                        Icon(Icons.Outlined.CheckCircle, null)
                    }
                }
            },
            onValueChange = {
                playlistUrl.value = it
            },
            modifier = Modifier.fillMaxWidth()
        )
    }
}
