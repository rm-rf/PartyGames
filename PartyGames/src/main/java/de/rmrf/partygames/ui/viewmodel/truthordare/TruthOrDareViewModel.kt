package de.rmrf.partygames.ui.viewmodel.truthordare

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.rmrf.partygames.data.truthordare.TruthOrDareRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TruthOrDareViewModel(
    private val repository: TruthOrDareRepository
) : ViewModel() {

    private val _state = mutableStateOf(TruthOrDareState())
    val state : State<TruthOrDareState> = _state

    init {
        viewModelScope.launch {
            _state.value = state.value.copy(
                availableTruth = repository.getAvailableTruthCount(),
                availableDare = repository.getAvailableDareCount()
            )
            if (state.value.availableTruth + state.value.availableDare == 0) {
                repository.initializeData()
            }
        }
    }


    fun onEvent(event: TruthOrDareEvent) {
        when(event) {
            is TruthOrDareEvent.InGameEvent -> {
                handleInGameEvents(event)
            }
            is TruthOrDareEvent.MenuEvent -> {
                handleGamePickEvent(event)
            }
        }
    }

    private fun handleInGameEvents(event: TruthOrDareEvent.InGameEvent) {
        if (state.value.isInMenu) {
            return
        }
        if (event is TruthOrDareEvent.InGameEvent.DoneButtonClick) {
            viewModelScope.launch {
                repository.updateTruthOrDareData(state.value.truthOrDareData ?: return@launch)
            }
        }
        viewModelScope.launch {
            _state.value = state.value.copy(
                availableTruth = repository.getAvailableTruthCount(),
                availableDare = repository.getAvailableDareCount(),
                isInMenu = true
            )
        }

    }

    private fun handleGamePickEvent(event: TruthOrDareEvent.MenuEvent) {
        if (!state.value.isInMenu) {
            return
        }
        when(event) {
            TruthOrDareEvent.MenuEvent.DareButtonClick -> getDare()
            TruthOrDareEvent.MenuEvent.RandomButtonClick -> getRandom()
            TruthOrDareEvent.MenuEvent.TruthButtonClick -> getTruth()
        }
    }

    private fun getTruth() {
        viewModelScope.launch {
            repository.getTruth().also {
                _state.value = state.value.copy(
                    truthOrDareData = it,
                    isInMenu = false
                )
            }
        }
    }

    private fun getDare() {
        viewModelScope.launch {
            _state.value = state.value.copy(
                truthOrDareData = repository.getDare(),
                isInMenu = false
            )
        }
    }

    private fun getRandom() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getRandom().also {
                _state.value = state.value.copy(
                    truthOrDareData = it,
                    isInMenu = false
                )
            }
        }
    }

}
