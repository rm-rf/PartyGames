package de.rmrf.partygames.ui.viewmodel.pantomime

import de.rmrf.partygames.data.entities.PantomimeData

data class PantomimeState(
    val pantomimeData: PantomimeData? = null,
    val isTimerButtonShown: Boolean = false,
    val isTaskShown: Boolean = false,
    val isTimerRunning: Boolean = false,
    val availableQuestions: Int = 0,
    val buttonState: PantomimeButtonState = PantomimeButtonState.Nothing,
)
