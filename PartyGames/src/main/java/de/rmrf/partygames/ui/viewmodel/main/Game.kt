package de.rmrf.partygames.ui.viewmodel.main

import androidx.annotation.StringRes
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector

sealed class Game(
    @StringRes open val name: Int,
    open val imageVector: @Composable () -> ImageVector,
    open var favorite: Boolean,
    open val preferencesKey: String
) {

    data class Pantomime(
        override val name: Int,
        override val imageVector: @Composable () -> ImageVector,
        override var favorite: Boolean
    ) : Game(name, imageVector, favorite, "pantomime")

    data class SpinTheBottle(
        override val name: Int,
        override val imageVector: @Composable () -> ImageVector,
        override var favorite: Boolean
    ) : Game(name, imageVector, favorite, "spin_the_bottle")

    data class NeverHaveIEver(
        override val name: Int,
        override val imageVector: @Composable () -> ImageVector,
        override var favorite: Boolean
    ) : Game(name, imageVector, favorite, "never_have_i_ever")

    data class WhoWouldRather(
        override val name: Int,
        override val imageVector: @Composable () -> ImageVector,
        override var favorite: Boolean
    ) : Game(name, imageVector, favorite, "who_would_rather")

    data class WouldYouRather(
        override val name: Int,
        override val imageVector: @Composable () -> ImageVector,
        override var favorite: Boolean

    ) : Game(name, imageVector, favorite, "would_you_rather")

    data class GuessTheSong(
        override val name: Int,
        override val imageVector: @Composable () -> ImageVector,
        override var favorite: Boolean
    ) : Game(name, imageVector, favorite, "guess_the_song")

    data class TruthOrDare(
        override val name: Int,
        override val imageVector: @Composable () -> ImageVector,
        override var favorite: Boolean
    ) : Game(name, imageVector, favorite, "truth_or_dare")
}
