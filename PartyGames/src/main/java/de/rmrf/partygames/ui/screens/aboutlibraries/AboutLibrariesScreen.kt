package de.rmrf.partygames.ui.screens.aboutlibraries

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import com.mikepenz.aboutlibraries.ui.compose.LibrariesContainer
import com.mikepenz.aboutlibraries.ui.compose.LibraryDefaults
import com.mikepenz.aboutlibraries.ui.compose.util.StableLibrary
import de.rmrf.partygames.BuildConfig
import de.rmrf.partygames.R
import de.rmrf.partygames.ui.components.DynamicHeightAlertDialog

@Composable
fun AboutLibrariesScreen() {

    val showOwnLicense = remember {
        mutableStateOf(false)
    }

    var currentLibrary by remember {
        mutableStateOf<StableLibrary?>(null)
    }

    val uriHandler = LocalUriHandler.current

    if (showOwnLicense.value) {
        DynamicHeightAlertDialog(
            onDismissRequest = { showOwnLicense.value = false },
            confirmButton = {},
            title = {
                Text(text = stringResource(id = R.string.third_party_libraries_own_license))
            },
            text = {
                Column {
                    Text(
                        text = stringResource(id = R.string.about_license_title),
                        modifier = Modifier.clickable {
                            uriHandler.openUri("https://www.gnu.org/licenses/gpl-3.0.txt")
                        },
                        textDecoration = TextDecoration.Underline,
                        color = MaterialTheme.colorScheme.primary
                    )
                    Text(text = "${stringResource(R.string.app_name)} ${stringResource(id = R.string.copyright_rmrf)}")
                }
            }
        )
    }

    AnimatedVisibility(
        visible = currentLibrary != null
    ) {
        currentLibrary?.let {
            DynamicHeightAlertDialog(
                onDismissRequest = { currentLibrary = null },
                confirmButton = {
                    TextButton(onClick = {
                        currentLibrary = null
                    }) {
                        Text(text = "Ok")
                    }
                },
                title = {
                    Text(text = it.library.name)
                },
                text = {
                    Text(text = it.library.licenses.firstOrNull()?.licenseContent ?: "")
                }
            )
        }
    }

    Box(modifier = Modifier.fillMaxSize()) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
            modifier = Modifier
        ) {
            Image(
                painterResource(id = R.drawable.ic_logo_foreground),
                contentDescription = null,
                modifier = Modifier
                    .background(
                        Color.White,
                        shape = CircleShape
                    )
                    .size(68.dp)
                    .padding(4.dp)
            )
            Text(
                text = stringResource(id = R.string.app_name),
                style = MaterialTheme.typography.headlineSmall,
                modifier = Modifier.padding(4.dp)
            )
            Spacer(modifier = Modifier.size(8.dp))
            Text(text = "Version ${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})")
            Spacer(modifier = Modifier.size(8.dp))
            OutlinedButton(
                onClick = {
                    showOwnLicense.value = true
                }
            ) {
                Text(text = stringResource(id = R.string.third_party_libraries_own_license))
            }
            Spacer(modifier = Modifier.size(8.dp))
            Box {
                LibrariesContainer(
                    modifier = Modifier.fillMaxWidth(),
                    colors = LibraryDefaults.libraryColors(
                        backgroundColor = MaterialTheme.colorScheme.surface,
                        contentColor = contentColorFor(backgroundColor = MaterialTheme.colorScheme.surface),
                        badgeBackgroundColor = MaterialTheme.colorScheme.primary,
                        badgeContentColor = contentColorFor(backgroundColor = MaterialTheme.colorScheme.primary),
                        dialogConfirmButtonColor = MaterialTheme.colorScheme.primary
                    ),
                    onLibraryClick = {
                        currentLibrary = it
                    },
                    contentPadding = PaddingValues(4.dp),
                    padding = LibraryDefaults.libraryPadding(),
                    showVersion = true,
                    showAuthor = true,
                )
            }
        }
    }
}
