package de.rmrf.partygames.ui.screens.about

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import de.rmrf.partygames.R
import de.rmrf.partygames.ui.screens.Screen
import de.rmrf.partygames.ui.screens.about.components.CardView

@Composable
fun AboutScreen(
    navController: NavController
) {
    BackHandler {
        navController.popBackStack(Screen.MainScreen.route, false)
    }

    val list = listOf(
        Triple(stringResource(R.string.about_truthordare), stringResource(R.string.about_truthordare_text), 0),
        Triple(stringResource(R.string.about_wouldyourather), stringResource(R.string.about_wouldyourather_text), 1),
        Triple(stringResource(R.string.about_whowouldrather), stringResource(R.string.about_whowouldrather_text), 2),
        Triple(stringResource(R.string.about_neverhaveiever), stringResource(R.string.about_neverhaveiever_text), 3),
        Triple(stringResource(R.string.about_pantomime), stringResource(R.string.about_pantomime_text), 4),
    )

    val selectedIndex = remember {
        mutableIntStateOf(-1)
    }

    LazyColumn(
        modifier = Modifier.fillMaxSize()
    ) {
        items(items = list) {
            CardView(
                title = it.first,
                text = it.second,
                isVisible = selectedIndex.intValue == it.third,
                modifier = Modifier.padding(5.dp)
            ) { newVisibility -> //Change visibility
                if (newVisibility){
                    selectedIndex.intValue = it.third
                } else {
                    selectedIndex.intValue = -1
                }
            }
        }
    }
}
