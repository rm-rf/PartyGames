package de.rmrf.partygames.ui.viewmodel.guessthesong

import de.rmrf.partygames.data.entities.Playlist

sealed class GuessTheSongEvent {

    data object ShowAnswer : GuessTheSongEvent()
    data object ActionIconClicked : GuessTheSongEvent()

    sealed class DialogEvent : GuessTheSongEvent() {
        data class AddPlaylist(val playlistId: String) : DialogEvent()
        data class DeletePlaylist(val playlist: Playlist) : DialogEvent()
        data class SelectPlaylist(val playlist: Playlist) : DialogEvent()
    }


    data object TimerStart : GuessTheSongEvent()
}
