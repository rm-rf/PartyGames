package de.rmrf.partygames.ui.viewmodel.pantomime

sealed class PantomimeEvent {
    data object CardClick : PantomimeEvent()
    data object TimerStart : PantomimeEvent()
    data object TimerFinish : PantomimeEvent()
}
