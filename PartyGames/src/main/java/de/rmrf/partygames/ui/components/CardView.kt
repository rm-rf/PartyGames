package de.rmrf.partygames.ui.components

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ShapeDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import de.rmrf.partygames.data.entities.LocalizedString
import de.rmrf.partygames.data.entities.getString

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CardView(
    localizedString: LocalizedString,
    modifier: Modifier = Modifier,
    @StringRes question: Int? = null,
    enableClick: Boolean = false,
    onCardClick: () -> Unit = {},
    isVisible: Boolean = true,
    notVisibleText: String? = null,
    width: Dp = 360.dp,
    height: Dp = 260.dp
) {
    Box(
        modifier = modifier.size(width, height)
    ) {
        Card(
            onClick = {if (enableClick) {onCardClick()}},
            modifier = modifier
                .align(alignment = Alignment.Center)
                .matchParentSize(),
            shape = ShapeDefaults.ExtraLarge
        ) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = modifier.fillMaxSize()
            ) {
                Text(
                    text =
                        if (isVisible) {
                            val string = if (question == null) { "" } else {
                                "${stringResource(id = question)} "
                            }
                            "${string}${localizedString.getString()}"
                        } else {
                            notVisibleText ?: return@Card
                        },
                    fontSize = 24.sp,
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}
