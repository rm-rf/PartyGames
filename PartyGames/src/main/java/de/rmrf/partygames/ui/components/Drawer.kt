package de.rmrf.partygames.ui.components

import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Translate
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import de.rmrf.partygames.R
import de.rmrf.partygames.ui.screens.Screen
import de.rmrf.partygames.ui.screens.main.components.DrawerItems
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
fun Drawer(
    navController: NavController,
    drawerState: DrawerState,
    scope: CoroutineScope,
    destination: String?,
    content: @Composable () -> Unit
) {
    val drawerScrollState = rememberScrollState()


    BackHandler(enabled = drawerState.isOpen) {
        scope.launch {
            drawerState.close()
        }
    }
    val context = LocalContext.current

    val translateDialog = remember {
        mutableStateOf<Boolean?>(null)
    }

    ModalNavigationDrawer(
        gesturesEnabled = destination?.equals(Screen.MainScreen.route) ?: false,
        drawerState = drawerState,
        drawerContent = {
            ModalDrawerSheet(
                modifier = Modifier
                    .width(320.dp)
                    .fillMaxHeight()
                    .verticalScroll(drawerScrollState)
            ) {
                DrawerItems {
                    when (it.id) {
                        1 -> { //Favorite Games
                            navController.navigate(Screen.PreferencesScreen.FavoritesScreen.route)
                            scope.launch {
                                drawerState.close()
                            }
                        }

                        2 -> { //Settings
                            navController.navigate(Screen.PreferencesScreen.PreferencesScreenDefault.route)
                            scope.launch {
                                drawerState.close()
                            }
                        }

                        4 -> { //About Screen
                            navController.navigate(Screen.AboutScreen.route)
                            scope.launch {
                                drawerState.close()
                            }
                        }

                        5 -> { //Libraries
                            navController.navigate(Screen.LibraryScreen.route)
                            scope.launch {
                                drawerState.close()
                            }
                        }

                        6 -> {
                            scope.launch {
                                drawerState.close()
                            }
                            translateDialog.value = true
                        }

                        else -> {
                            Toast.makeText(context, it.label, Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        },
        content = {
            val uriHandler = LocalUriHandler.current
            translateDialog.value?.let {
                DynamicHeightAlertDialog(
                    onDismissRequest = {
                        translateDialog.value = null
                    },
                    title = {
                        Text(text = stringResource(id = R.string.all_translate))
                    },
                    text = {
                        Text(text = stringResource(id = R.string.all_redirect_to_weblate))
                    },
                    icon = {
                        Icon(
                            imageVector = Icons.Outlined.Translate,
                            contentDescription = stringResource(id = R.string.all_translate)
                        )
                    },
                    confirmButton = {
                        TextButton(
                            onClick = {
                                uriHandler.openUri(context.getString(R.string.all_weblate_url))
                                translateDialog.value = null
                            }
                        ) {
                            Text(text = context.getString((R.string.drawer_open_weblate))) //TODO: Extract String resource
                        }
                    },
                    dismissButton = {
                        TextButton(
                            onClick = {
                                translateDialog.value = null
                            }
                        ) {
                            Text(text = stringResource(id = R.string.all_cancel))
                        }
                    }
                )
            }
            content()
        }
    )
}
