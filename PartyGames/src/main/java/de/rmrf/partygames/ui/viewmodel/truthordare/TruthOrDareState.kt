package de.rmrf.partygames.ui.viewmodel.truthordare

import de.rmrf.partygames.data.entities.TruthOrDareData

data class TruthOrDareState(
    val truthOrDareData: TruthOrDareData? = null,
    val availableDare: Int = 0,
    val availableTruth: Int = 0,
    val isInMenu: Boolean = true
)
