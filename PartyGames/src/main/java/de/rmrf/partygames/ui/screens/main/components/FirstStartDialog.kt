package de.rmrf.partygames.ui.screens.main.components

import android.content.Context
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalUriHandler
import androidx.compose.ui.res.stringResource
import de.rmrf.partygames.R
import de.rmrf.partygames.di.KoinModule
import de.rmrf.partygames.preferences.dataStorePreferences
import de.rmrf.partygames.ui.components.DynamicHeightAlertDialog
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.inject

@Composable
fun FirstStartDialog() {
    val uriHandler = LocalUriHandler.current
    val koinModule = KoinModule()
    DynamicHeightAlertDialog(
        onDismissRequest = {
            koinModule.dismissDialog()
        },
        title = {
            Text(stringResource(R.string.startup_thanks_for_updating))
        },
        text = {
            Text(stringResource(R.string.startup_update_about))
        },
        dismissButton = {
            TextButton(onClick = { koinModule.dismissDialog() }) {
                Text(stringResource(R.string.all_ok))
            }
        },
        confirmButton = {
            TextButton(
                onClick = {
                    koinModule.dismissDialog()
                    uriHandler.openUri("https://codeberg.org/rm-rf/PartyGames/releases")
                }
            ) {
                Text(stringResource(R.string.startup_more_info))
            }

        }
    )
}


private fun KoinModule.dismissDialog() {
    val scope = CoroutineScope(Dispatchers.Main)
    val dataStorePreferences = inject<Context>().value.dataStorePreferences
    scope.launch {
        dataStorePreferences.firstStart.saveValue(false)
    }
}
