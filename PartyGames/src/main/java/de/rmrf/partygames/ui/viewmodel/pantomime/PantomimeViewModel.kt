package de.rmrf.partygames.ui.viewmodel.pantomime


import android.content.Context
import android.os.CountDownTimer
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import de.rmrf.partygames.data.pantomime.PantomimeRepository
import de.rmrf.partygames.preferences.dataStorePreferences
import de.rmrf.partygames.util.TimerBaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.inject

class PantomimeViewModel(
    private val repository: PantomimeRepository
) : TimerBaseViewModel() {

    private val _state = mutableStateOf(PantomimeState())
    val state: State<PantomimeState> = _state


    init {
        viewModelScope.launch {
            val timerValue = inject<Context>().value.dataStorePreferences.pantomimeTimerValue.getValue().toLong()
            millisInFuture =
                if (timerValue >= 5) {
                    timerValue * 1000
                } else {
                    inject<Context>().value.dataStorePreferences.pantomimeTimerValue.defaultValue.toLong()
                }

            timeData.value = millisInFuture
            getPantomime()
        }
    }

    private fun getPantomime() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.getPantomime().also {
                _state.value = state.value.copy(
                    pantomimeData = it,
                    isTimerButtonShown = false,
                    isTaskShown = false,
                    isTimerRunning = false
                )
            }
            repository.getAvailablePantomimeCount().also {
                _state.value = state.value.copy(
                    availableQuestions = it
                )
            }
        }
    }

    fun onButtonEvent(event: PantomimeButtonEvent) {
        when (event) {
            PantomimeButtonEvent.DoneButtonClick -> {
                viewModelScope.launch {
                    repository.updatePantomime(state.value.pantomimeData ?: return@launch)
                    cancelTimer()
                    getPantomime()
                }
                _state.value = state.value.copy(
                    buttonState = PantomimeButtonState.Nothing
                )

            }

            PantomimeButtonEvent.NoButtonClick -> {
                viewModelScope.launch {
                    if (!inject<Context>().value.dataStorePreferences.enableOversteppedPantomimeCountsAsUsed.getValue()) {
                        repository.updatePantomime(state.value.pantomimeData ?: return@launch)
                    }
                    getPantomime()
                }
                _state.value = state.value.copy(
                    buttonState = PantomimeButtonState.Nothing
                )
            }

            PantomimeButtonEvent.SkipButtonClick -> {
                viewModelScope.launch {
                    if (!inject<Context>().value.dataStorePreferences.enableOversteppedPantomimeCountsAsUsed.getValue()) {
                        repository.updatePantomime(state.value.pantomimeData ?: return@launch)
                    }
                    getPantomime()
                }
                _state.value = state.value.copy(
                    buttonState = PantomimeButtonState.Nothing
                )
            }

            PantomimeButtonEvent.YesButtonClick -> {
                viewModelScope.launch {
                    repository.updatePantomime(state.value.pantomimeData ?: return@launch)
                    getPantomime()
                }
                _state.value = state.value.copy(
                    buttonState = PantomimeButtonState.Nothing
                )
            }
        }
    }

    fun onEvent(event: PantomimeEvent) {
        when (event) {
            PantomimeEvent.CardClick -> {
                if (state.value.isTimerButtonShown || state.value.isTimerRunning) {
                    return
                } else if (!state.value.isTaskShown) {
                    _state.value = state.value.copy(
                        isTaskShown = true,
                        isTimerButtonShown = true,
                        buttonState = PantomimeButtonState.SkipVisible
                    )
                    return
                }
                timeData.value = millisInFuture
            }

            PantomimeEvent.TimerFinish -> {
                _state.value = state.value.copy(
                    isTimerRunning = false,
                    isTaskShown = true,
                    buttonState = PantomimeButtonState.TimerFinished
                )
            }

            PantomimeEvent.TimerStart -> {
                _state.value = state.value.copy(
                    isTimerRunning = true,
                    isTaskShown = false,
                    isTimerButtonShown = false,
                    buttonState = PantomimeButtonState.DoneVisible
                )
                startTimer()
            }
        }
    }


    override fun createTimerObject(): CountDownTimer {
        return object : CountDownTimer(millisInFuture, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timeData.value = millisUntilFinished
            }

            override fun onFinish() {
                onEvent(PantomimeEvent.TimerFinish)
                running.value = false
            }
        }
    }


}
