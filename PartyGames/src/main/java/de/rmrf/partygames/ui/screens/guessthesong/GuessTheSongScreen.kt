package de.rmrf.partygames.ui.screens.guessthesong

import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.MusicNote
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.navigation.NavController
import de.rmrf.partygames.R
import de.rmrf.partygames.preferences.dataStorePreferences
import de.rmrf.partygames.ui.components.Countdown
import de.rmrf.partygames.ui.components.DynamicHeightAlertDialog
import de.rmrf.partygames.ui.components.Spacer
import de.rmrf.partygames.ui.screens.Screen
import de.rmrf.partygames.ui.screens.guessthesong.components.CardView
import de.rmrf.partygames.ui.screens.guessthesong.components.FullscreenPlaylistManagementDialog
import de.rmrf.partygames.ui.viewmodel.guessthesong.GuessTheSongEvent
import de.rmrf.partygames.ui.viewmodel.guessthesong.GuessTheSongViewModel
import de.rmrf.partygames.util.isFalse
import de.rmrf.partygames.util.isTrue
import org.koin.androidx.compose.koinViewModel

/**
 * Displays the Guess The Song screen.
 *
 * @param onActionClicked The callback function to be called when the action icon is clicked.
 * @param viewModel The GuessTheSongViewModel instance to handle the logic of the screen. Default is a new instance obtained from KoinViewModel.
 */
@Composable
fun GuessTheSongScreen(
    navController: NavController,
    onActionClicked: MutableState<() -> Unit>,
    viewModel: GuessTheSongViewModel = koinViewModel()
) {
    val state = viewModel.state.value


    BackHandler {
        viewModel.dismissDialog()
        navController.popBackStack(Screen.MainScreen.route, inclusive = false)
    }

    onActionClicked.value = {
        viewModel.onEvent(GuessTheSongEvent.ActionIconClicked)
    }

    val firstStart = remember {
        mutableStateOf(false)
    }
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {
        firstStart.value = context.dataStorePreferences.firstGuesstheSongStart.getValue()
        context.dataStorePreferences.firstGuesstheSongStart.saveValue(false)
    }

    if (firstStart.value){
        DynamicHeightAlertDialog(
            onDismissRequest = {
            },
            title = {
                Text(stringResource(R.string.all_new_version_info))
            },
            text = {
                Text(text = stringResource(R.string.guessthesong_alert_text))
            },
            confirmButton = {
                TextButton(onClick = { firstStart.value = false }) {
                    Text(stringResource(R.string.all_ok))
                }
            }
        )
    }

    AnimatedVisibility(visible = state.showSongPickerDialog && !firstStart.value) {
        Dialog(
            onDismissRequest = {},
            properties = DialogProperties(usePlatformDefaultWidth = false)
        ) {
            Surface(modifier = Modifier.fillMaxSize()) {
                FullscreenPlaylistManagementDialog(
                    title = {
                        Text(text = "Manage Playlists")
                    },
                    playlists = state.availablePlaylist,
                    onEvent = {
                        viewModel.onEvent(it)
                    },
                    currentSelectedPlaylist = state.currentPlaylist,
                    onDismiss = {
                        viewModel.dismissDialog()
                        if (!it && state.currentPlaylist == null) {
                            navController.popBackStack(Screen.MainScreen.route, inclusive = false)
                        }

                    },
                    isLoading = state.isAddingPlaylist
                )
            }
        }
    }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxSize()
    ) {
        state.isSongLoading.isTrue {
            LinearProgressIndicator(
                modifier = Modifier.fillMaxWidth()
            )
        }.isFalse {
            Spacer(dp = 4.dp)
        }
        Spacer(dp = 8.dp)
        Text(
            text = state.currentPlaylist?.playlistName.orEmpty(),
            fontSize = 24.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(dp = 4.dp)

        Box {
            Icon(
                Icons.Outlined.MusicNote,
                null,
                modifier = Modifier.size(180.dp)
            )
        }
        Spacer(8.dp)

        Countdown(
            cancelTimer = {
                viewModel.cancelTimer()
            },
            onTimerStart = {
                viewModel.onEvent(GuessTheSongEvent.TimerStart)
            },
            isButtonVisible = !state.isSongLoading && !viewModel.running.value && !state.showSolution,
            timeDataState = viewModel.timeData
        )
        Spacer(dp = 4.dp)
        CardView(
            song = state,
        )
        Spacer(dp = 4.dp)
        Button(
            onClick = {
                viewModel.onEvent(GuessTheSongEvent.ShowAnswer)
            }
        ) {
            Text(stringResource(R.string.all_show_answer))
        }
    }
}

