package de.rmrf.partygames.ui.viewmodel.wouldyourather

import android.content.Context
import android.os.CountDownTimer
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import de.rmrf.partygames.data.wouldyourather.WouldYouRatherRepository
import de.rmrf.partygames.preferences.dataStorePreferences
import de.rmrf.partygames.util.TimerBaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.core.component.inject

class WouldYouRatherViewModel(
    private val repository: WouldYouRatherRepository
) : TimerBaseViewModel() {

    private val _state = mutableStateOf(WouldYouRatherState())
    val state: State<WouldYouRatherState> = _state

    init {
        viewModelScope.launch {
            val timerValue = inject<Context>().value.dataStorePreferences.wouldYouRatherTimerValue.getValue().toLong()
            millisInFuture =
                if (timerValue >= 5) {
                    (timerValue + 1) * 1000
                } else {
                    inject<Context>().value.dataStorePreferences.wouldYouRatherTimerValue.defaultValue.toLong()
                }

            timeData.value = millisInFuture
            _state.value = state.value.copy(
                isTimerVisible = inject<Context>().value.dataStorePreferences.enableWouldYouRatherTimer.getValue()
            )
        }
        getWouldYouRather()
    }

    private fun getWouldYouRather() {
        if (state.value.isTimerVisible) {
            startTimer()
        }
        viewModelScope.launch(Dispatchers.IO) {
            repository.getWouldYouRather().also {
                _state.value = state.value.copy(
                    wouldYouRatherData = it,
                    isTimerRunning = false,
                )
            }
            repository.getAvailableWouldYouRatherCount().also {
                _state.value = state.value.copy(
                    availableQuestions = it
                )
            }
        }
    }

    fun onButtonEvent(event: WouldYouRatherButtonEvent) {
        when (event) {
            WouldYouRatherButtonEvent.DoneButtonClick -> {
                viewModelScope.launch {
                    repository.updateWouldYouRatherData(state.value.wouldYouRatherData ?: return@launch)
                    cancelTimer()
                    getWouldYouRather()
                }
            }

            WouldYouRatherButtonEvent.SkipButtonClick -> {
                cancelTimer()
                getWouldYouRather()
            }
        }
    }

    fun onEvent(event: WouldYouRatherEvent) {
        when (event) {
            WouldYouRatherEvent.TimerFinish -> {
                _state.value = state.value.copy(
                    isTimerRunning = false
                )
                getWouldYouRather()
            }

            WouldYouRatherEvent.TimerStart -> {
                _state.value = state.value.copy(
                    isTimerRunning = true
                )
                startTimer()
            }
        }
    }


    override fun createTimerObject(): CountDownTimer {
        return object : CountDownTimer(millisInFuture, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timeData.value = millisUntilFinished
            }

            override fun onFinish() {
                onEvent(WouldYouRatherEvent.TimerFinish)
                running.value = false
            }
        }
    }
}
