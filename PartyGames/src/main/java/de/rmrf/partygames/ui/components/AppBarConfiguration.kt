package de.rmrf.partygames.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.outlined.ArrowBack
import androidx.compose.material.icons.outlined.LibraryMusic
import androidx.compose.material.icons.outlined.Menu
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import de.rmrf.partygames.R
import de.rmrf.partygames.ui.screens.Screen
import de.rmrf.partygames.ui.viewmodel.main.AppBarState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ConfigureAppBar(
    navController: NavController,
    scope: CoroutineScope,
    drawerState: DrawerState,
    destination: String?,
    appBarScrollBehavior: TopAppBarScrollBehavior,
    onActionClick: State<() -> Unit>,
    updateAppBar: (AppBarState) -> Unit
) {
    val context = LocalContext.current
    LaunchedEffect(key1 = destination) {
        updateAppBar(
            AppBarState(
                title = when (destination) {
                    Screen.PantomimeScreen.route -> context.getString(R.string.name_pantomime)
                    Screen.SpinTheBottleScreen.route -> context.getString(R.string.name_spinthebottle)
                    Screen.NeverHaveIEverScreen.route -> context.getString(R.string.name_neverhaveiever)
                    Screen.WhoWouldRatherScreen.route -> context.getString(R.string.name_whowouldrather)
                    Screen.WouldYouRatherScreen.route -> context.getString(R.string.name_wouldyourather)
                    Screen.PreferencesScreen.FavoritesScreen.route -> context.getString(R.string.favorite_games)
                    Screen.PreferencesScreen.PreferencesMainScreen.route -> context.getString(R.string.preference_all_title)
                    Screen.PreferencesScreen.PreferencesUIScreen.route -> context.getString(R.string.all_ui)
                    Screen.PreferencesScreen.PreferencesScreenGames.route -> context.getString(R.string.all_game_settings)
                    Screen.LibraryScreen.route -> context.getString(R.string.all_third_party_libraries)
                    Screen.AboutScreen.route -> context.getString(R.string.about)
                    else -> context.getString(R.string.app_name)
                },
                actions = {
                    when (destination) {
                        Screen.MainScreen.route -> {
                            if (appBarScrollBehavior.state.overlappedFraction != 0f) {
                                IconButton(onClick = {
                                    if (0.6f > appBarScrollBehavior.state.overlappedFraction && appBarScrollBehavior.state.overlappedFraction > 0.5f) {
                                        throw IllegalStateException("It's not a bug, it's a feature!")
                                    }
                                }) {
                                    Image(
                                        painter = painterResource(id = R.drawable.ic_logo_foreground),
                                        contentDescription = null,
                                        modifier = Modifier
                                            .alpha(appBarScrollBehavior.state.overlappedFraction)
                                            .size(48.dp)
                                    )
                                }
                            }
                        }

                        Screen.GuessTheSongScreen.route -> {
                            IconButton(
                                onClick = {
                                    onActionClick.value()
                                }
                            ) {
                                Icon(imageVector = Icons.Outlined.LibraryMusic, contentDescription = null)
                            }
                        }

                        else -> {

                        }
                    }
                },
                navigationIcon = {
                    when (destination) {
                        Screen.MainScreen.route -> {
                            IconButton(
                                onClick = {
                                    scope.launch {
                                        drawerState.open()
                                    }
                                }
                            ) {
                                Icon(Icons.Outlined.Menu, null)
                            }
                        }

                        Screen.PreferencesScreen.FavoritesScreen.route -> {
                            IconButton(
                                onClick = {
                                    navController.navigateUp()
                                }
                            ) {
                                Icon(Icons.AutoMirrored.Outlined.ArrowBack, null)
                            }
                        }

                        Screen.PreferencesScreen.PreferencesScreenGames.route -> {
                            IconButton(
                                onClick = {
                                    navController.navigateUp()
                                }
                            ) {
                                Icon(imageVector = Icons.AutoMirrored.Outlined.ArrowBack, contentDescription = null)
                            }
                        }

                        Screen.PreferencesScreen.PreferencesUIScreen.route -> {
                            IconButton(
                                onClick = {
                                    navController.navigateUp()
                                }
                            ) {
                                Icon(imageVector = Icons.AutoMirrored.Outlined.ArrowBack, contentDescription = null)
                            }
                        }

                        else -> {
                            IconButton(
                                onClick = {
                                    navController.popBackStack(Screen.MainScreen.route, false)
                                }
                            ) {
                                Icon(Icons.AutoMirrored.Outlined.ArrowBack, null)
                            }
                        }
                    }
                }
            )
        )
    }
}
