package de.rmrf.partygames.ui.screens.main.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.*
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import de.rmrf.partygames.R
import de.rmrf.partygames.ui.viewmodel.main.Game

@Composable
fun GameGrid(
    navController: NavController,
    items: List<Game>,
    modifier: Modifier = Modifier,
    updateFavorite: (Game) -> Unit
) {
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        contentPadding = PaddingValues(8.dp),
        modifier = modifier.fillMaxSize()
    ) {
        header {
            StartPageHeader()
        }
        if (items.any { it.favorite }) {
            header {
                Text(stringResource(R.string.main_favorites))
            }
            items(items.filter { it.favorite }, navController, updateFavorite)
            header {
                Text(stringResource(R.string.all_games))
            }
        }
        items(items, navController, updateFavorite)
    }
}

fun LazyGridScope.header(
    content: @Composable LazyGridItemScope.() -> Unit
) {
    item(span = { GridItemSpan(this.maxLineSpan) }, content = content)
}


@OptIn(ExperimentalMaterial3Api::class)
fun LazyGridScope.items(
    items: List<Game>,
    navController: NavController,
    updateFavorite: (Game) -> Unit
) {
    items(items) {
        val configuration = LocalConfiguration.current
        val windowWidth = configuration.screenWidthDp
        val cardWidth = (windowWidth / 2 - 16).dp
        Card(
            onClick = {
                handleNavigation(navController, it)
            },
            modifier = Modifier
                .size(width = cardWidth, height = cardWidth.plus(40.dp))
                .padding(4.dp),
        ) {
            FavoriteButton(updateFavorite = updateFavorite, game = it)
            Box(
                modifier = Modifier.align(Alignment.CenterHorizontally),
                contentAlignment = Alignment.Center
            ) {
                Icon(
                    imageVector = it.imageVector(),
                    null,
                    modifier = Modifier
                        .size(120.dp)

                )
            }
            Text(
                stringResource(id = it.name),
                fontSize = 24.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth()
            )
        }
    }
}
