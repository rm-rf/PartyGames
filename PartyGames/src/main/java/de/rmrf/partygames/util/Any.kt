package de.rmrf.partygames.util


/**
 * Executes the specified [block] when called on a `null` object in a coroutine.
 *
 * @param block The code to execute when called on a `null` object.
 */
suspend fun Any?.isNull(
    block: suspend () -> Unit
) {
    if (this == null) {
        block()
    }
}


/**
 * Executes the specified [block] when called on a `null` object.
 *
 * @param block The code to execute when called on a `null` object.
 */
fun Any?.isNull(
    block: () -> Unit
) {
    if (this == null) {
        block()
    }
}
