package de.rmrf.partygames.util

import android.content.Context

inline fun <reified T> Context.getArray(
    name: String
): Array<T> {
    val resId = resources.getIdentifier(name, "array", packageName)
    return resources.obtainTypedArray(resId).let { array ->
        Array(array.length()) { i ->
            array.getText(i) as T
        }.also { array.recycle() }
    }
}

fun Context.getString(
    name: String
): String {
    val resId = resources.getIdentifier(name, "string", packageName)
    return resources.getString(resId)
}
