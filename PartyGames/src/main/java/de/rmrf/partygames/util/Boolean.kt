package de.rmrf.partygames.util

import androidx.compose.runtime.Composable

/**
 * Checks if the boolean value is true and executes the provided composable function if true.
 *
 * @param content the composable function to execute if the boolean value is true
 * @return the original boolean value
 */
@Composable
@Deprecated("This is bad code, do not use >:(", replaceWith = ReplaceWith(""), DeprecationLevel.WARNING)
fun Boolean.isTrue(content: @Composable () -> Unit): Boolean {
    if (this) {
        content()
    }
    return this
}

/**
 * Checks if the boolean value is false and executes the provided composable function if false.
 *
 * @param content the composable function to be executed if the boolean value is false.
 * @return the boolean value itself.
 */
@Composable
fun Boolean.isFalse(content: @Composable () -> Unit): Boolean {
    if (!this) {
        content()
    }
    return this
}
