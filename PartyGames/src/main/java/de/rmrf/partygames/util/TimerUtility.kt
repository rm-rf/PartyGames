package de.rmrf.partygames.util

import java.util.concurrent.TimeUnit


fun Long.formatTime(): String = String.format(
    "%02d:%02d",
    TimeUnit.MILLISECONDS.toMinutes(this),
    TimeUnit.MILLISECONDS.toSeconds(this) % 60
)
