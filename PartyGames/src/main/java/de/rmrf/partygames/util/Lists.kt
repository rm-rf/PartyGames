package de.rmrf.partygames.util

fun <T> List<T>.isEvenLength(): Boolean {
    return this.size % 2 == 0
}
