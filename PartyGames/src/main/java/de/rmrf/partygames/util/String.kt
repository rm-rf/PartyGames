package de.rmrf.partygames.util

import java.util.regex.Pattern

/**
 * Parses the URL and extracts the value of the "list" query parameter.
 *
 * @return The value of the "list" query parameter if it exists, or null otherwise.
 */
fun String.urlParser(): String? {
    val regex = "[&?]list=([^&]+)"

    val pattern = Pattern.compile(regex)
    val matcher = pattern.matcher(this)

    if (matcher.find()) {
        return matcher.group(1)
    }
    return null

}
