package de.rmrf.partygames.util

import android.os.CountDownTimer
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableLongStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import org.koin.core.component.KoinComponent

abstract class TimerBaseViewModel : ViewModel(), KoinComponent {


    /**
     * This needs to be set in the subClass
     * */
    protected open var millisInFuture: Long = 0
    open var timeData: MutableState<Long> = mutableLongStateOf(millisInFuture)

    protected open var countDownTimer: CountDownTimer? = null
    open var running: MutableState<Boolean> = mutableStateOf(false)


    protected abstract fun createTimerObject(): CountDownTimer


    fun startTimer() {
        timeData.value = millisInFuture


        if (countDownTimer != null) {
            countDownTimer?.cancel()
            countDownTimer = null
        }

        timeData.value = millisInFuture

        countDownTimer = createTimerObject()

        countDownTimer?.start()
        running.value = true
    }

    fun cancelTimer() {
        countDownTimer?.cancel()
        timeData.value = millisInFuture
        running.value = false
    }

    override fun onCleared() {
        super.onCleared()
        countDownTimer?.cancel()
    }

}
