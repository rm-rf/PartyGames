package de.rmrf.partygames.di


import androidx.room.Room
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import de.rmrf.partygames.data.entities.SongData
import de.rmrf.partygames.data.entities.api.PipedAPI
import de.rmrf.partygames.data.entities.db.PartyGamesDataBase
import de.rmrf.partygames.data.entities.db.dao.*
import de.rmrf.partygames.data.neverhaveiever.NeverHaveIEverRepository
import de.rmrf.partygames.data.neverhaveiever.NeverHaveIEverRepositoryImpl
import de.rmrf.partygames.data.pantomime.PantomimeRepository
import de.rmrf.partygames.data.pantomime.PantomimeRepositoryImpl
import de.rmrf.partygames.data.song.SongRepository
import de.rmrf.partygames.data.song.SongRepositoryImpl
import de.rmrf.partygames.data.truthordare.TruthOrDareRepository
import de.rmrf.partygames.data.truthordare.TruthOrDareRepositoryImpl
import de.rmrf.partygames.data.whowouldrather.WhoWouldRatherRepository
import de.rmrf.partygames.data.whowouldrather.WhoWouldRatherRepositoryImpl
import de.rmrf.partygames.data.wouldyourather.WouldYouRatherRepository
import de.rmrf.partygames.data.wouldyourather.WouldYouRatherRepositoryImpl
import de.rmrf.partygames.ui.viewmodel.guessthesong.GuessTheSongViewModel
import de.rmrf.partygames.ui.viewmodel.main.MainViewModel
import de.rmrf.partygames.ui.viewmodel.neverhaveiever.NeverHaveIEverViewModel
import de.rmrf.partygames.ui.viewmodel.pantomime.PantomimeViewModel
import de.rmrf.partygames.ui.viewmodel.truthordare.TruthOrDareViewModel
import de.rmrf.partygames.ui.viewmodel.whowouldrather.WhoWouldRatherViewModel
import de.rmrf.partygames.ui.viewmodel.wouldyourather.WouldYouRatherViewModel
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.component.KoinComponent
import org.koin.dsl.module
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

private val json = Json {
    ignoreUnknownKeys = true
}//Json { ignoreUnknownKeys = true }

@OptIn(ExperimentalSerializationApi::class)
val appModule = module {
    single<PantomimeRepository> { PantomimeRepositoryImpl(androidContext(), get<PantomimeDao>()) }
    single<NeverHaveIEverRepository> { NeverHaveIEverRepositoryImpl(androidContext(), get<NeverHaveIEverDao>()) }
    single<WhoWouldRatherRepository> { WhoWouldRatherRepositoryImpl(androidContext(), get<WhoWouldRatherDao>()) }
    single<WouldYouRatherRepository> { WouldYouRatherRepositoryImpl(androidContext(), get<WouldYouRatherDao>()) }
    single<SongRepository> { SongRepositoryImpl(get<SongDao>()) }
    single<TruthOrDareRepository> { TruthOrDareRepositoryImpl(androidContext(), get<TruthOrDareDao>()) }

    single<Retrofit> {
        Retrofit.Builder()
            .client(OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS)
                    .build()
            ).baseUrl(SongData.API_URL)
            .addConverterFactory(json.asConverterFactory(MediaType.get("application/json")))
            .build()
    }


    single<PipedAPI> {
        get<Retrofit>().create(PipedAPI::class.java)
    }

    viewModel { PantomimeViewModel(get<PantomimeRepository>()) }
    viewModel { MainViewModel(androidContext()) }
    viewModel { NeverHaveIEverViewModel(get<NeverHaveIEverRepository>()) }
    viewModel { WhoWouldRatherViewModel(get<WhoWouldRatherRepository>()) }
    viewModel { WouldYouRatherViewModel(get<WouldYouRatherRepository>()) }
    viewModel { GuessTheSongViewModel(get<SongRepository>()) }
    viewModel { TruthOrDareViewModel(get<TruthOrDareRepository>()) }

    single<PartyGamesDataBase> {
        Room.databaseBuilder(
            androidContext(), PartyGamesDataBase::class.java, PartyGamesDataBase.DATABASE_NAME
        ).fallbackToDestructiveMigration().build()
    }
    single<PantomimeDao> {
        val database = get<PartyGamesDataBase>()
        database.pantomimeDao()
    }
    single<NeverHaveIEverDao> {
        val database = get<PartyGamesDataBase>()
        database.neverHaveIEverDao()
    }
    single<WhoWouldRatherDao> {
        val database = get<PartyGamesDataBase>()
        database.whoWouldRatherDao()
    }
    single<WouldYouRatherDao> {
        val database = get<PartyGamesDataBase>()
        database.wouldYouRatherDao()
    }
    single<SongDao> {
        val database = get<PartyGamesDataBase>()
        database.songDao()
    }
    single<TruthOrDareDao> {
        val dataBase = get<PartyGamesDataBase>()
        dataBase.truthOrDareDao()
    }
}

class KoinModule : KoinComponent
