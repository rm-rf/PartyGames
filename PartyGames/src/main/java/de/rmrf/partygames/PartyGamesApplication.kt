package de.rmrf.partygames

import android.app.Application
import de.rmrf.partygames.di.appModule
import de.rmrf.partygames.preferences.dataStorePreferences
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * This class represents the main application class for the Party Games Application.
 * It extends the Android [Application] class and initializes Koin dependency injection.
 *
 * @see Application
 */
class PartyGamesApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@PartyGamesApplication)
            modules(appModule)
        }

        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            val appVersions = dataStorePreferences.appVersions.getValue()
            if (!appVersions.contains(BuildConfig.VERSION_NAME)) {
                dataStorePreferences.firstStart.saveValue(true)
                dataStorePreferences.appVersions.saveValue(appVersions.plus(BuildConfig.VERSION_NAME))
            }
        }

    }
}
