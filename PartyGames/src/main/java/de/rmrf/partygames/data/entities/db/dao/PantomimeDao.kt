package de.rmrf.partygames.data.entities.db.dao

import androidx.room.*
import de.rmrf.partygames.data.entities.PantomimeData

@Dao
interface PantomimeDao {
    /**
     * Save Pantomime Data pantomimeData
     */
    @Insert
    suspend fun insertPantomimeData(pantomimeData: List<PantomimeData>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updatePantomimeData(pantomimeData: PantomimeData)

    @Query("SELECT * FROM pantomime")
    suspend fun getAllData(): List<PantomimeData>

    @Query("SELECT COUNT(*) FROM pantomime WHERE used = 0")
    suspend fun getUnusedCount(): Int

    @Delete
    suspend fun deletePantomimeData(vararg pantomimeData: PantomimeData)

}
