package de.rmrf.partygames.data.entities.api.piped

import kotlinx.serialization.Serializable

@Serializable
data class PipedAudioStream(
    val bitrate: Int,
    val codec: String,
    val format: String,
    val indexEnd: Int,
    val indexStart: Int,
    val initEnd: Int,
    val initStart: Int,
    val mimeType: String,
    val quality: String,
    val url: String,
    val videoOnly: Boolean,

)
