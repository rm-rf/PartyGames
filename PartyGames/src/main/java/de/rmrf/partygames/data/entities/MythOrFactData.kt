package de.rmrf.partygames.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import de.rmrf.partygames.data.entities.MythOrFactData.Companion.TABLE_NAME

@Entity(tableName = TABLE_NAME)
data class MythOrFactData(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val mythOrFact: LocalizedString,
    val isMyth: Boolean,
    val used: Boolean,

) {
    companion object {
        const val ARRAY_NAME_MYTH = "array_mythorfact_myths"
        const val ARRAY_NAME_FACT = "array_mythorfact_facts"
        const val TABLE_NAME = "mythorfact"
    }
}
