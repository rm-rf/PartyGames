package de.rmrf.partygames.data.pantomime

import de.rmrf.partygames.data.entities.PantomimeData

interface PantomimeRepository {
    suspend fun getPantomime(): PantomimeData
    suspend fun initializeData()
    suspend fun resetData()
    suspend fun updatePantomime(pantomimeData: PantomimeData)
    suspend fun getAvailablePantomimeCount(): Int
}
