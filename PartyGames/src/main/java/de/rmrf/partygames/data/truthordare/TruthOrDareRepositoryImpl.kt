package de.rmrf.partygames.data.truthordare

import android.content.Context
import de.rmrf.partygames.data.entities.LocalizedString
import de.rmrf.partygames.data.entities.TruthOrDareData
import de.rmrf.partygames.data.entities.db.dao.TruthOrDareDao
import de.rmrf.partygames.util.getArray
import kotlinx.coroutines.runBlocking
import kotlin.random.Random

class TruthOrDareRepositoryImpl(private val context: Context, truthOrDareDao: TruthOrDareDao) : TruthOrDareRepository {

    private val _truthOrDareDao = truthOrDareDao

    override suspend fun getTruth(): TruthOrDareData {
        if (_truthOrDareDao.getUnusedTruthCount() == 0) {
            runBlocking {
                resetTruthData()
            }
            return getTruth()
        }
        val truths = _truthOrDareDao.getTruths().filter {
            !it.used
        }
        return truths.random()
    }

    override suspend fun getDare(): TruthOrDareData {
        if (_truthOrDareDao.getUnusedDareCount() == 0) {
            runBlocking {
                resetDareData()
            }
            return getDare()
        }
        val dares = _truthOrDareDao.getDares().filter {
            !it.used
        }
        return dares.random()
    }

    override suspend fun getRandom(): TruthOrDareData {
        return if (Random.nextBoolean()) {
            getTruth()
        } else {
            getDare()
        }
    }

    override suspend fun initializeData() {
        val truths = context.getArray<String>(TruthOrDareData.ARRAY_NAME_TRUTH)
        val dares = context.getArray<String>(TruthOrDareData.ARRAY_NAME_DARE)
        if (_truthOrDareDao.getUnusedTruthCount() + _truthOrDareDao.getUnusedDareCount() == 0) {
            _truthOrDareDao.insertTruthOrDare(
                truths.indices.map {
                    TruthOrDareData.newTruth(
                        truth = LocalizedString(
                            arrayName = TruthOrDareData.ARRAY_NAME_TRUTH,
                            it
                        )
                    )
                }
            )
            //TODO: FIX YOUR FUCKING CODE
            _truthOrDareDao.insertTruthOrDare(
                dares.indices.map {
                    TruthOrDareData.newDare(
                        dare = LocalizedString(
                            arrayName = TruthOrDareData.ARRAY_NAME_DARE,
                            it
                        )
                    )
                }
            )
        }
    }


    override suspend fun resetTruthData() {
        val data: List<TruthOrDareData> = _truthOrDareDao.getTruths()
        if (data.isNotEmpty()) {
            data.forEach {
                _truthOrDareDao.updateTruthOrDare(it.copy(used = false))
            }
        }
    }

    override suspend fun resetDareData() {
        val data: List<TruthOrDareData> = _truthOrDareDao.getDares()
        if (data.isNotEmpty()) {
            data.forEach {
                _truthOrDareDao.updateTruthOrDare(it.copy(used = false))
            }
        }
    }


    override suspend fun updateTruthOrDareData(truthOrDareData: TruthOrDareData) {
        _truthOrDareDao.updateTruthOrDare(truthOrDareData.copy(used = true))
    }

    override suspend fun getAvailableTruthCount(): Int {
        return _truthOrDareDao.getUnusedTruthCount()
    }

    override suspend fun getAvailableDareCount(): Int {
        return _truthOrDareDao.getUnusedDareCount()
    }
}
