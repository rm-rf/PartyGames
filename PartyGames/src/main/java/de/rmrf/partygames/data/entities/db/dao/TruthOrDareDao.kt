package de.rmrf.partygames.data.entities.db.dao

import androidx.room.*
import de.rmrf.partygames.data.entities.TruthOrDareData

@Dao
interface TruthOrDareDao {

    @Insert
    suspend fun insertTruthOrDare(items: List<TruthOrDareData>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateTruthOrDare(vararg items: TruthOrDareData)

    @Query("SELECT * FROM truth_or_dare_data WHERE todState = 0") //TODState = 0 == TRUTH
    suspend fun getTruths() : List<TruthOrDareData>

    @Query("SELECT * FROM truth_or_dare_data WHERE todState = 1") //TODState = 1 == DARE
    suspend fun getDares() : List<TruthOrDareData>

    @Query("SELECT COUNT(*) FROM truth_or_dare_data WHERE used = 0 AND todState = 0")
    suspend fun getUnusedTruthCount() : Int

    @Query("SELECT COUNT(*) FROM truth_or_dare_data WHERE used = 0 AND todState = 1")
    suspend fun getUnusedDareCount() : Int
}
