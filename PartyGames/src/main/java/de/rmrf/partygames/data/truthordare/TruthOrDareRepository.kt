package de.rmrf.partygames.data.truthordare

import de.rmrf.partygames.data.entities.TruthOrDareData

interface TruthOrDareRepository {
    suspend fun getTruth(): TruthOrDareData

    suspend fun getDare(): TruthOrDareData

    suspend fun getRandom(): TruthOrDareData
    suspend fun initializeData()
    suspend fun resetTruthData()

    suspend fun resetDareData()
    suspend fun updateTruthOrDareData(truthOrDareData: TruthOrDareData)
    suspend fun getAvailableTruthCount(): Int

    suspend fun getAvailableDareCount():  Int
}
