package de.rmrf.partygames.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = TruthOrDareData.TABLE_NAME)
data class TruthOrDareData(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val data: LocalizedString,
    val todState: TODState,
    val used: Boolean
) {


    companion object {
        const val TABLE_NAME = "truth_or_dare_data"
        const val ARRAY_NAME_TRUTH = "truths"
        const val ARRAY_NAME_DARE = "dares"

        fun newDare(dare: LocalizedString) : TruthOrDareData {
            return TruthOrDareData(
                data = dare,
                todState = TODState.DARE,
                used = false
            )
        }

        fun newTruth(truth: LocalizedString) : TruthOrDareData {
            return TruthOrDareData(
                data = truth,
                todState = TODState.TRUTH,
                used = false
            )
        }
    }
}

enum class TODState {
    TRUTH,
    DARE
}
