package de.rmrf.partygames.data.neverhaveiever

import de.rmrf.partygames.data.entities.NeverHaveIEverData

interface NeverHaveIEverRepository {

    suspend fun getNeverHaveIEver(): NeverHaveIEverData
    suspend fun initializeData()
    suspend fun resetData()
    suspend fun updateNeverHaveIEver(neverHaveIEverData: NeverHaveIEverData)
    suspend fun getAvailableNeverHaveIEverCount(): Int

}
