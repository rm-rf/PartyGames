package de.rmrf.partygames.data.entities

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import de.rmrf.partygames.util.getArray
import kotlinx.serialization.Serializable

@Serializable
data class LocalizedString(
    val arrayName: String,
    val index: Int
)


@Composable
fun LocalizedString.getString(): String {
    return LocalContext.current.getArray<String>(arrayName)[index]
}
