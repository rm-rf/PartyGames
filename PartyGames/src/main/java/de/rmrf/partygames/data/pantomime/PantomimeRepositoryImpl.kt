package de.rmrf.partygames.data.pantomime

import android.content.Context
import de.rmrf.partygames.data.entities.LocalizedString
import de.rmrf.partygames.data.entities.PantomimeData
import de.rmrf.partygames.data.entities.db.dao.PantomimeDao
import de.rmrf.partygames.util.getArray
import kotlinx.coroutines.runBlocking


class PantomimeRepositoryImpl(private val context: Context, private val pantomimeDao: PantomimeDao) :
    PantomimeRepository {

    private val _pantomime = pantomimeDao

    override suspend fun getPantomime(): PantomimeData {

        if (_pantomime.getUnusedCount() == 0) {
            runBlocking {
                resetData()
            }
            return getPantomime()
        }
        val pantomime = _pantomime.getAllData().filter {
            !it.used
        }
        return pantomime.random()
    }


    override suspend fun initializeData() {
        val array = context.getArray<String>(PantomimeData.ARRAY_NAME)
        if (_pantomime.getUnusedCount() == 0) {
            _pantomime.insertPantomimeData(
                array.indices.map {
                    PantomimeData(
                        id = it,
                        pantomime = LocalizedString(
                            arrayName = PantomimeData.ARRAY_NAME,
                            it,
                        ),
                        used = false
                    )
                }
            )
        }
    }

    override suspend fun resetData() {
        val pantomime = _pantomime.getAllData()
        if (pantomime.isNotEmpty()) {
            pantomime.forEach {
                _pantomime.updatePantomimeData(it.copy(used = false))
            }
        } else {
            initializeData()
        }
    }

    override suspend fun updatePantomime(pantomimeData: PantomimeData) {
        _pantomime.updatePantomimeData(pantomimeData.copy(used = true))
    }


    override suspend fun getAvailablePantomimeCount(): Int {
        return _pantomime.getUnusedCount()
    }
}
