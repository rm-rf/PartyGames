package de.rmrf.partygames.data.neverhaveiever

import android.content.Context
import de.rmrf.partygames.data.entities.LocalizedString
import de.rmrf.partygames.data.entities.NeverHaveIEverData
import de.rmrf.partygames.data.entities.db.dao.NeverHaveIEverDao
import de.rmrf.partygames.util.getArray

class NeverHaveIEverRepositoryImpl(private val context: Context, private val neverHaveIEverDao: NeverHaveIEverDao) :
    NeverHaveIEverRepository {

    private val _neverhaveiever = neverHaveIEverDao

    override suspend fun getNeverHaveIEver(): NeverHaveIEverData {
        if (_neverhaveiever.getUnusedCount() == 0) {
            resetData()
            return getNeverHaveIEver()
        }
        val neverhaveiever = _neverhaveiever.getAllData().filter {
            !it.used
        }
        return neverhaveiever.random()
    }

    override suspend fun initializeData() {
        val array = context.getArray<String>(NeverHaveIEverData.ARRAY_NAME)
        if (_neverhaveiever.getUnusedCount() == 0) {
            _neverhaveiever.insertNeverHaveIEverData(
                array.indices.map {
                    NeverHaveIEverData(
                        id = it,
                        neverhaveiever = LocalizedString(
                            arrayName = NeverHaveIEverData.ARRAY_NAME,
                            it,
                        ),
                        used = false
                    )
                }
            )
        }
    }

    override suspend fun resetData() {
        val neverhaveiever = _neverhaveiever.getAllData()
        if (neverhaveiever.isNotEmpty()) {
            neverhaveiever.forEach {
                _neverhaveiever.updateNeverHaveIEverData(it.copy(used = false))
            }
        } else {
            initializeData()
        }
    }

    override suspend fun updateNeverHaveIEver(neverHaveIEverData: NeverHaveIEverData) {
        _neverhaveiever.updateNeverHaveIEverData(neverHaveIEverData.copy(used = true))
    }

    override suspend fun getAvailableNeverHaveIEverCount(): Int {
        return _neverhaveiever.getUnusedCount()
    }
}
