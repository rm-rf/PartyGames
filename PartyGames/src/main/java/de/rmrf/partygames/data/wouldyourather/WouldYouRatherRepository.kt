package de.rmrf.partygames.data.wouldyourather

import de.rmrf.partygames.data.entities.WouldYouRatherData

interface WouldYouRatherRepository {
    suspend fun getWouldYouRather(): WouldYouRatherData
    suspend fun initializeData()
    suspend fun resetData()
    suspend fun updateWouldYouRatherData(wouldYouRatherData: WouldYouRatherData)
    suspend fun getAvailableWouldYouRatherCount(): Int
}
