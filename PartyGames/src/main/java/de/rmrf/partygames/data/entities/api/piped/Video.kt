package de.rmrf.partygames.data.entities.api.piped

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Video(
    val title: String,
    @SerialName("uploader") val author: String,
    val audioStreams: List<PipedAudioStream>
)
