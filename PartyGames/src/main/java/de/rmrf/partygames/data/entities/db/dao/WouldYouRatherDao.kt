package de.rmrf.partygames.data.entities.db.dao

import androidx.room.*
import de.rmrf.partygames.data.entities.WouldYouRatherData

@Dao
interface WouldYouRatherDao {
    @Insert
    suspend fun insertWouldYouRatherData(wouldYouRatherData: List<WouldYouRatherData>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateWouldYouRatherData(wouldYouRatherData: WouldYouRatherData)

    @Query("SELECT * FROM wouldyourather")
    suspend fun getAllData(): List<WouldYouRatherData>

    @Query("SELECT COUNT(*) FROM wouldyourather WHERE used = 0")
    suspend fun getUnusedCount(): Int

    @Delete
    suspend fun deleteWouldYouRatherData(vararg wouldYouRatherData: WouldYouRatherData)
}
