package de.rmrf.partygames.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import de.rmrf.partygames.data.entities.NeverHaveIEverData.Companion.ARRAY_NAME

@Entity(tableName = ARRAY_NAME)
data class NeverHaveIEverData(
    @PrimaryKey
    val id: Int,
    val neverhaveiever: LocalizedString,
    val used: Boolean
) {
    companion object {
        const val ARRAY_NAME = "neverhaveiever"
    }
}
