package de.rmrf.partygames.data.whowouldrather

import android.content.Context
import de.rmrf.partygames.data.entities.LocalizedString
import de.rmrf.partygames.data.entities.WhoWouldRatherData
import de.rmrf.partygames.data.entities.db.dao.WhoWouldRatherDao
import de.rmrf.partygames.util.getArray
import kotlinx.coroutines.runBlocking

class WhoWouldRatherRepositoryImpl(private val context: Context, private val whoWouldRatherDao: WhoWouldRatherDao) :
    WhoWouldRatherRepository {

    private val _whowouldrather = whoWouldRatherDao

    override suspend fun getWhoWouldRather(): WhoWouldRatherData {
        if (_whowouldrather.getUnusedCount() == 0) {
            runBlocking {
                resetData()
            }
            return getWhoWouldRather()
        }
        val whowouldrather = _whowouldrather.getAllData().filter {
            !it.used
        }
        return whowouldrather.random()
    }

    override suspend fun initializeData() {
        val array = context.getArray<String>(WhoWouldRatherData.ARRAY_NAME)
        if (_whowouldrather.getUnusedCount() == 0) {
            _whowouldrather.insertWhoWouldRatherData(
                array.indices.map {
                    WhoWouldRatherData(
                        id = it,
                        whowouldrather = LocalizedString(
                            arrayName = WhoWouldRatherData.ARRAY_NAME,
                            it,
                        ),
                        used = false
                    )
                }
            )
        }
    }

    override suspend fun resetData() {
        val whowouldrather = _whowouldrather.getAllData()
        if (whowouldrather.isNotEmpty()) {
            whowouldrather.forEach {
                _whowouldrather.updateWhoWouldRatherData(it.copy(used = false))
            }
        } else {
            initializeData()
        }
    }

    override suspend fun updateWhoWouldRather(whoWouldRatherData: WhoWouldRatherData) {
        _whowouldrather.updateWhoWouldRatherData(whoWouldRatherData.copy(used = true))
    }

    override suspend fun getAvailableWhoWouldRatherCount(): Int {
        return _whowouldrather.getUnusedCount()
    }
}
