package de.rmrf.partygames.data.whowouldrather

import de.rmrf.partygames.data.entities.WhoWouldRatherData

interface WhoWouldRatherRepository {
    suspend fun getWhoWouldRather(): WhoWouldRatherData
    suspend fun initializeData()
    suspend fun resetData()
    suspend fun updateWhoWouldRather(whoWouldRatherData: WhoWouldRatherData)
    suspend fun getAvailableWhoWouldRatherCount(): Int
}
