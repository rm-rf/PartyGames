package de.rmrf.partygames.data.entities.db

import androidx.room.TypeConverter
import de.rmrf.partygames.data.entities.LocalizedString
import de.rmrf.partygames.data.entities.TODState
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class Converters {
    @TypeConverter
    fun fromLocalizedString(localizedString: LocalizedString): String {
        return Json.encodeToString(localizedString)
    }

    @TypeConverter
    fun toLocalizedString(string: String): LocalizedString {
        return Json.decodeFromString(string)
    }

    @TypeConverter
    fun toHealth(value: Int) = enumValues<TODState>()[value]

    @TypeConverter
    fun fromHealth(value: TODState) = value.ordinal

}
