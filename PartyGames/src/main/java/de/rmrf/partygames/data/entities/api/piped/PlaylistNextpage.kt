package de.rmrf.partygames.data.entities.api.piped

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PlaylistNextpage(
    val nextpage: String?,
    @SerialName("relatedStreams") val items: List<PlaylistItem>,
)
