package de.rmrf.partygames.data.entities.api

import de.rmrf.partygames.data.entities.api.piped.Playlist
import de.rmrf.partygames.data.entities.api.piped.PlaylistNextpage
import de.rmrf.partygames.data.entities.api.piped.Video
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query


interface PipedAPI {

    @Headers(
        "User-Agent: PartyGamesApplication"
    )
    @GET("/streams/{videoId}")
    suspend fun getVideo(
        @Path("videoId") videoId: String
    ) : Video

    @Headers(
        "User-Agent: PartyGamesApplication"
    )
    @GET("/playlists/{playlistId}")
    suspend fun getPlaylist(
        @Path("playlistId") playlistId: String
    ) : Playlist

    @Headers(
        "User-Agent: PartyGamesApplication"
    )
    @GET("/nextpage/playlists/{playlistId}")
    suspend fun getNextPage(
        @Path("playlistId") playlistId: String,
        @Query("nextpage") nextpage: String
    ) : PlaylistNextpage


}
