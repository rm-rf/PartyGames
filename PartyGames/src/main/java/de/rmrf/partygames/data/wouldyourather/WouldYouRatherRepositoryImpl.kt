package de.rmrf.partygames.data.wouldyourather

import android.content.Context
import de.rmrf.partygames.data.entities.LocalizedString
import de.rmrf.partygames.data.entities.WouldYouRatherData
import de.rmrf.partygames.data.entities.db.dao.WouldYouRatherDao
import de.rmrf.partygames.util.getArray
import kotlinx.coroutines.runBlocking

class WouldYouRatherRepositoryImpl(private val context: Context, private val wouldYouRatherDao: WouldYouRatherDao) :
    WouldYouRatherRepository {

    private val _wouldyourather = wouldYouRatherDao

    override suspend fun getWouldYouRather(): WouldYouRatherData {
        if (_wouldyourather.getUnusedCount() == 0) {
            runBlocking {
                resetData()
            }
            return getWouldYouRather()
        }
        val wouldYouRather = _wouldyourather.getAllData().filter {
            !it.used
        }
        return wouldYouRather.random()
    }

    override suspend fun initializeData() {
        val array = context.getArray<String>(WouldYouRatherData.ARRAY_NAME)
        if (_wouldyourather.getUnusedCount() == 0) {
            _wouldyourather.insertWouldYouRatherData(
                array.indices.map {
                    WouldYouRatherData(
                        id = it,
                        wouldyourather = LocalizedString(
                            arrayName = WouldYouRatherData.ARRAY_NAME,
                            it,
                        ),
                        used = false
                    )
                }
            )
        }
    }

    override suspend fun resetData() {
        val wouldYouRather = _wouldyourather.getAllData()
        if (wouldYouRather.isNotEmpty()) {
            wouldYouRather.forEach {
                _wouldyourather.updateWouldYouRatherData(it.copy(used = false))
            }
        } else {
            initializeData()
        }
    }

    override suspend fun updateWouldYouRatherData(wouldYouRatherData: WouldYouRatherData) {
        _wouldyourather.updateWouldYouRatherData(wouldYouRatherData.copy(used = true))
    }

    override suspend fun getAvailableWouldYouRatherCount(): Int {
        return _wouldyourather.getUnusedCount()
    }
}
