package de.rmrf.partygames.data.entities.api.piped

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class Playlist(
    val name: String,
    val nextpage: String?,
    @SerialName("relatedStreams") val items: List<PlaylistItem>,
    @SerialName("videos") val videoCount: Int
)
