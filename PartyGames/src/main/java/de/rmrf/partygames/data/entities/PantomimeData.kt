package de.rmrf.partygames.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import de.rmrf.partygames.data.entities.PantomimeData.Companion.ARRAY_NAME
import kotlinx.serialization.Serializable

@Serializable
@Entity(tableName = ARRAY_NAME)
data class PantomimeData(
    @PrimaryKey
    val id: Int,
    val pantomime: LocalizedString,
    val used: Boolean
) {
    companion object {
        const val ARRAY_NAME = "pantomime"
    }
}
