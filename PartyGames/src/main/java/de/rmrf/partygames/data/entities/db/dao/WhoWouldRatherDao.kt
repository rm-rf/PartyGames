package de.rmrf.partygames.data.entities.db.dao

import androidx.room.*
import de.rmrf.partygames.data.entities.WhoWouldRatherData

@Dao
interface WhoWouldRatherDao {
    @Insert
    suspend fun insertWhoWouldRatherData(whoWouldRatherData: List<WhoWouldRatherData>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateWhoWouldRatherData(whoWouldRatherData: WhoWouldRatherData)

    @Query("SELECT * FROM whowouldrather")
    suspend fun getAllData(): List<WhoWouldRatherData>

    @Query("SELECT COUNT(*) FROM whowouldrather WHERE used = 0")
    suspend fun getUnusedCount(): Int

    @Delete
    suspend fun deleteWhoWouldRatherData(vararg whoWouldRatherData: WhoWouldRatherData)
}
