package de.rmrf.partygames.data.entities.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import de.rmrf.partygames.data.entities.*
import de.rmrf.partygames.data.entities.db.dao.*

@Database(
    entities = [
        PantomimeData::class,
        NeverHaveIEverData::class,
        WhoWouldRatherData::class,
        WouldYouRatherData::class,
        SongData::class,
        Playlist::class,
        TruthOrDareData::class
    ],
    version = 1
)
@TypeConverters(Converters::class)
abstract class PartyGamesDataBase : RoomDatabase() {
    abstract fun pantomimeDao(): PantomimeDao

    abstract fun neverHaveIEverDao(): NeverHaveIEverDao

    abstract fun whoWouldRatherDao(): WhoWouldRatherDao

    abstract fun wouldYouRatherDao(): WouldYouRatherDao

    abstract fun songDao(): SongDao

    abstract fun truthOrDareDao(): TruthOrDareDao

    companion object {
        const val DATABASE_NAME = "pg_database.db"
    }
}
