package de.rmrf.partygames.data.song

import de.rmrf.partygames.data.entities.Playlist
import de.rmrf.partygames.data.entities.SongData

interface SongRepository {
    suspend fun resetData(playlistId: String)
    suspend fun addPlaylist(playlistId: String)
    suspend fun updateSongData(songData: SongData)
    suspend fun getAvailableSongCount(playlistId: String): Int
    suspend fun getSong(playlistId: String): SongData
    suspend fun deletePlaylist(playlistId: String)

    suspend fun getBestStream(songData: SongData) : String
    suspend fun getAllPlaylitst() : List<Playlist>
}
