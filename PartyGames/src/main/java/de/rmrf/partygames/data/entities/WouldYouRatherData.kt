package de.rmrf.partygames.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import de.rmrf.partygames.data.entities.WouldYouRatherData.Companion.ARRAY_NAME

@Entity(tableName = ARRAY_NAME)
data class WouldYouRatherData(
    @PrimaryKey
    val id: Int,
    val wouldyourather: LocalizedString,
    val used: Boolean
) {
    companion object {
        const val ARRAY_NAME = "wouldyourather"
    }
}
