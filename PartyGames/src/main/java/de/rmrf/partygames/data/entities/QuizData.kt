package de.rmrf.partygames.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import de.rmrf.partygames.data.entities.QuizData.Companion.ARRAY_NAME

@Entity(tableName = ARRAY_NAME)
data class QuizData(
    @PrimaryKey
    val id: Int,
    val quiz: LocalizedString,
    val rightAnswer: LocalizedString,
    val wrongAnswer1: LocalizedString,
    val wrongAnswer2: LocalizedString,
    val wrongAnswer3: LocalizedString,
    val used: Boolean
) {
    companion object {

        //Assuming the arrays have the same lenth
        const val ARRAY_NAME = "quiz"
        const val ARRAY_NAME_R = "right_answer"
        const val ARRAY_NAME_W1 = "wrong_answer_1"
        const val ARRAY_NAME_W2 = "wrong_answer_2"
        const val ARRAY_NAME_W3 = "wrong_answer_3"
        //const val ARRAY_NAME_W1 = "wrongAnswer1"
    }
}
