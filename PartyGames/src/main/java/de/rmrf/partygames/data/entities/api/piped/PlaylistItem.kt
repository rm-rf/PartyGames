package de.rmrf.partygames.data.entities.api.piped

import kotlinx.serialization.Serializable

@Serializable
data class PlaylistItem(
    val url: String
) {
    fun toVideoId() : String {
        return url.removePrefix("/watch?v=")
    }
}
