package de.rmrf.partygames.data.entities.db.dao

import androidx.room.*
import de.rmrf.partygames.data.entities.Playlist
import de.rmrf.partygames.data.entities.SongData

@Dao
interface SongDao {
    @Insert
    suspend fun insertSongData(songData: List<SongData>)

    @Insert
    suspend fun instertPlaylist(playlist: Playlist)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateSongData(songData: SongData)

    @Query("SELECT * FROM musicIds")
    suspend fun getAllData(): List<SongData>

    @Query("SELECT * FROM playlists")
    suspend fun getAllPlaylists(): List<Playlist>

    @Query("SELECT * FROM playlists WHERE playlistId = :playlistId")
    suspend fun getPlaylistById(playlistId: String): Playlist

    @Query("SELECT * FROM musicIds WHERE playlistId = :playlistId")
    suspend fun getPlaylistItems(playlistId: String): List<SongData>

    @Query("SELECT COUNT(*) FROM musicIds WHERE used = 0 AND playlistId = :playlistId")
    suspend fun getUnusedCount(playlistId: String): Int

    @Delete
    suspend fun deleteSongData(vararg songData: SongData)

    @Delete
    suspend fun deletePlaylist(vararg playlist: Playlist)
}
