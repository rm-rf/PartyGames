package de.rmrf.partygames.data.entities.db.dao

import androidx.room.*
import de.rmrf.partygames.data.entities.NeverHaveIEverData

/**
 * Never have i ever dao unsed by the Room Database
 */
@Dao
interface NeverHaveIEverDao {
    /**
     * Insert never have i ever data
     *
     * @param neverHaveIEverData
     */
    @Insert
    suspend fun insertNeverHaveIEverData(neverHaveIEverData: List<NeverHaveIEverData>)

    /**
     * Update never have i ever data
     *
     * @param neverHaveIEverData data to update
     */
    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateNeverHaveIEverData(neverHaveIEverData: NeverHaveIEverData)

    /**
     * Get all data
     *
     * @return A list of all NeverhaveIEverData in the DatabaseTable
     */
    @Query("SELECT * FROM neverhaveiever")
    fun getAllData(): List<NeverHaveIEverData>

    /**
     * Get unused count
     *
     * @return The count of all unused NeverHaveIEverData objects in the Database
     */
    @Query("SELECT COUNT(*) FROM neverhaveiever WHERE used = 0")
    suspend fun getUnusedCount(): Int

    /**
     * Delete never have i ever data
     *
     * @param neverHaveIEverData data to delete
     */
    @Delete
    fun deleteNeverHaveIEverData(vararg neverHaveIEverData: NeverHaveIEverData)
}
