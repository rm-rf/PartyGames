package de.rmrf.partygames.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import de.rmrf.partygames.data.entities.SongData.Companion.ARRAY_NAME
import de.rmrf.partygames.data.entities.api.PipedAPI
import de.rmrf.partygames.data.entities.api.piped.Video

@Entity(tableName = ARRAY_NAME)
data class SongData(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val videoId: String,
    val playlistId: String,
    val used: Boolean
) {
    companion object {
        const val ARRAY_NAME = "musicIds"
        const val API_URL = "https://pipedapi.kavin.rocks/"
    }
    suspend fun videoFromId(api: PipedAPI) : Video {
        return api.getVideo(videoId)
    }

}

@Entity(tableName = "playlists")
data class Playlist(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val playlistName: String,
    val playlistId: String,
    val videoCount: Int
)
