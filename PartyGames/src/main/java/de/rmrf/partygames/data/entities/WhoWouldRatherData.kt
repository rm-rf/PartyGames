package de.rmrf.partygames.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import de.rmrf.partygames.data.entities.WhoWouldRatherData.Companion.ARRAY_NAME

@Entity(tableName = ARRAY_NAME)
data class WhoWouldRatherData(
    @PrimaryKey
    val id: Int,
    val whowouldrather: LocalizedString,
    val used: Boolean
) {
    companion object {
        const val ARRAY_NAME = "whowouldrather"
    }
}
