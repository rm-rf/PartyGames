package de.rmrf.partygames.data.song

import de.rmrf.partygames.data.entities.Playlist
import de.rmrf.partygames.data.entities.SongData
import de.rmrf.partygames.data.entities.api.PipedAPI
import de.rmrf.partygames.data.entities.db.dao.SongDao
import de.rmrf.partygames.util.isNull
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SongRepositoryImpl(songDao: SongDao) : SongRepository, KoinComponent {
    private val _song = songDao


    override suspend fun getSong(playlistId: String): SongData {
        val song = _song.getPlaylistItems(playlistId).filter {
            !it.used
        }
        return song.random()
    }

    override suspend fun deletePlaylist(playlistId: String) {
        _song.deleteSongData(songData = _song.getPlaylistItems(playlistId).toTypedArray())
        _song.deletePlaylist(_song.getPlaylistById(playlistId))
    }

    override suspend fun getBestStream(songData: SongData): String {
        return inject<PipedAPI>().value.getVideo(songData.videoId).audioStreams.maxBy {
            it.quality.filter { c -> c.isDigit() }.toInt()
        }.url

    }

    override suspend fun getAllPlaylitst(): List<Playlist> {
        return _song.getAllPlaylists()
    }


    override suspend fun resetData(playlistId: String) {
        val song = _song.getPlaylistItems(playlistId)
        if (song.isNotEmpty()) {
            song.forEach {
                _song.updateSongData(it.copy(used = false))
            }
        }
    }

    override suspend fun addPlaylist(playlistId: String) {
        val playlist = _song.getAllPlaylists().find {
            it.playlistId == playlistId
        }
        playlist.isNull(
            block = suspend {
                val response = inject<PipedAPI>().value.getPlaylist(playlistId)
                _song.instertPlaylist(
                    Playlist(
                        playlistId = playlistId,
                        playlistName = response.name,
                        videoCount = response.videoCount
                    )
                )
                val videoList = response.items.toMutableList()
                var nextpage = response.nextpage
                while (nextpage != null) {
                    val innerResponse = inject<PipedAPI>().value.getNextPage(playlistId, nextpage)
                    nextpage = innerResponse.nextpage
                    videoList.addAll(innerResponse.items)
                }
                _song.insertSongData(
                    videoList.map {
                        SongData(
                            videoId = it.toVideoId(),
                            used = false,
                            playlistId = playlistId
                        )
                    }
                )
            }
        )
    }

    override suspend fun updateSongData(songData: SongData) {
        _song.updateSongData(songData.copy(used = true))
    }

    override suspend fun getAvailableSongCount(playlistId: String): Int {
        return _song.getUnusedCount(playlistId)
    }

}
