# PartyGames 
<img src="logo.svg" alt="PartyGames Logo"/>

## A small lightweight app for PartyGames including:
* #### Truth or Dare
* #### Who would rather?
* #### Would you rather?
* #### Myth or fact?
* #### Never have I...
* #### Pantomime
* #### Spin the Bottle
###### and more in the future... ---> [ROADMAP](https://codeberg.org/rm-rf/PartyGames/src/branch/master/ROADMAP.md)

## [Changelog](https://codeberg.org/rm-rf/PartyGames/src/branch/master/CHANGELOG.md)



<p align="center">
<a href="https://f-droid.org/packages/de.rmrf.partygames">
<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    width="180px">
</a>
<br>
<a rel="me" href="https://social.tchncs.de/@rmrf">
<img src="mastodonLogo.svg"
 width="100px">
<br>
</a>
<br>

# Contributing:
* For bug reports, feature request or anything else feel free to open an issue!
* #### More questions and ideas for new games are always very welcome!!
* If you want to contribute code, you can open a pull request.
* Translate the app on Weblate:
<br>
<p align="center">
<a href="https://translate.codeberg.org/engage/partygames/">
<img src="https://translate.codeberg.org/widgets/partygames/-/multi-auto.svg" alt="Translations" /> 
</a> <br> 

You can also request your language and translate PartyGames in your language.


## Supported Android:
#### Android 5.0+

<img src="featureGraphic.svg" alt="FeatureGraphic.svg">