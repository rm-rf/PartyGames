import re

QUIZ_QUESTION = "QQ_"
QUIZ_RIGHT_ANSWER = "QRA_"
QUIZ_WRONG_ANSWER1 = "QWA1_"
QUIZ_WRONG_ANSWER2 = "QWA2_"
QUIZ_WRONG_ANSWER3 = "QWA3_"


def fetch_data(file_location, start_point):
    file = open(file_location)
    lines = file.readlines()

    for i in range(0, len(lines)):
        lines[i] = (re.sub("(Frage: |Richtige Antwort: |Falsche Antwort1?2?3?: |\n)", "", lines[i]))
        lines[i] = re.sub("\'", "\\'", lines[i])
        lines[i] = re.sub("\"", "\\\"", lines[i])

    output = ""
    ii = 0
    t = start_point
    iii = 0
    while ii < len(lines):
        if iii == 0:
            output = output + f"""<string name="{QUIZ_QUESTION}{t}">{lines[ii]}</string>\n"""
        if iii == 1:
            output = output + f"""<string name="{QUIZ_RIGHT_ANSWER}{t}">{lines[ii]}</string>\n"""
        if iii == 2:
            output = output + f"""<string name="{QUIZ_WRONG_ANSWER1}{t}">{lines[ii]}</string>\n"""
        if iii == 3:
            output = output + f"""<string name="{QUIZ_WRONG_ANSWER2}{t}">{lines[ii]}</string>\n"""
        if iii == 4:
            output = output + f"""<string name="{QUIZ_WRONG_ANSWER3}{t}">{lines[ii]}</string>\n"""
        ii += 1
        iii += 1
        if iii == 6:
            iii = 0
            t += 1
    print(output)

    quiz_header = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<resources>\n"
    quiz_questions = "    <string-array name=\"quiz_question\">\n"
    quiz_right_answer = "    <string-array name=\"right_answer\">\n"
    quiz_wrong_answer_1 = "    <string-array name=\"wrong_answer_1\">\n"
    quiz_wrong_answer_2 = "    <string-array name=\"wrong_answer_2\">\n"
    quiz_wrong_answer_3 = "    <string-array name=\"wrong_answer_3\">\n"
    quiz_footer = "</resources>"

    for i in range(0, t + 1):
        quiz_questions = quiz_questions + f"""        <item>@string/{QUIZ_QUESTION}{i}</item>\n"""
        quiz_right_answer = quiz_right_answer + f"""        <item>@string/{QUIZ_RIGHT_ANSWER}{i}</item>\n"""
        quiz_wrong_answer_1 = quiz_wrong_answer_1 + f"""        <item>@string/{QUIZ_WRONG_ANSWER1}{i}</item>\n"""
        quiz_wrong_answer_2 = quiz_wrong_answer_2 + f"""        <item>@string/{QUIZ_WRONG_ANSWER2}{i}</item>\n"""
        quiz_wrong_answer_3 = quiz_wrong_answer_3 + f"""        <item>@string/{QUIZ_WRONG_ANSWER3}{i}</item>\n"""

    quiz_questions = quiz_questions + "    </string-array>\n"
    quiz_right_answer = quiz_right_answer + "    </string-array>\n"
    quiz_wrong_answer_1 = quiz_wrong_answer_1 + "    </string-array>\n"
    quiz_wrong_answer_2 = quiz_wrong_answer_2 + "    </string-array>\n"
    quiz_wrong_answer_3 = quiz_wrong_answer_3 + "    </string-array>\n"

    output2 = quiz_header \
              + quiz_questions \
              + quiz_right_answer \
              + quiz_wrong_answer_1 \
              + quiz_wrong_answer_2 \
              + quiz_wrong_answer_3 \
              + quiz_footer

    print(output2)

    quiz = open('../PartyGames/src/main/res/values/quiz.xml', 'w')
    quiz.write(output2)
    quiz.close()


if __name__ == '__main__':
    fetch_data(input("Path: "), int(input("Question number: ")))
