<b>Play popular PartyGames including:</b>
<ol>
    <li>Truth or dare?</li>
    <li>Who would rather?</li>
    <li>Would you rather?</li>
    <li>Myth or fact?</li>
    <li>Never have I...</li>
    <li>Pantomime</li>
</ol>

<b>Gamemodes:</b> In myth or fact you can play against yourself or against others. If you pick the right answer you get points. If the answer is wrong you loose some. Also you can activate a timer for more pressure. You can activate the timer also in truth or dare and would you rather.

<b>Different questions in Truth or dare:</b> In truth or dare you can add different questions for a different play feeling.

<b>Please note</b> that this project is a <b>work in progress project.</b> This means that some questions may be missing, because they have not yet been added, even if the game itself would already work.

More <b>features</b> are added soon: https://codeberg.org/rm-rf/PartyGames/src/branch/master/ROADMAP.md