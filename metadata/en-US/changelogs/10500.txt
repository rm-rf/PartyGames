# v1.5.0
- Player management has been revised.
 - Players are now stored in a data class.

- UI Changes:
 - Added navigation drawer for better accessibility.
 - Updated font to rubik
 - Added a SplashScreen
 - updated app theme

- LicenseView:
 - Added own app license and font license
